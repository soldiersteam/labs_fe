import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {Observable} from 'rxjs/Observable';
import {Message} from '../models/message.model';
import {Conversation} from '../models/conversation.model';
import {Subject} from 'rxjs/Subject';
import {Http} from '@angular/http';
import {Router} from '@angular/router';
import {Course} from '../models/course.model';
import {NotificationsService} from 'angular2-notifications/dist';
import {SolutionGroup} from '../models/solution-group.model';
import {User} from '../models/user.model';

@Injectable()
export class ConversationService extends BaseService {
  protected conversationsSubject = new Subject<Conversation[]>();
  public readonly conversationsObservable = this.conversationsSubject.asObservable();
  public readonly pendingSubject = new Subject<boolean>();

  private pending = false;
  private course = undefined;

  public readonly courseSubject = new Subject<number>();

  constructor(http: Http, router: Router, notificationsService: NotificationsService) {
    super(http, router, notificationsService);

    const pendingSource = this.pendingSubject
      .map(pending => {
        this.pending = pending
      });

    this.courseSubject
      .map(courseId => {
        this.course = courseId;
      })
      .merge(pendingSource)
      .subscribe(() => this.index());
  }

  private getParams() {
    return {pending: this.pending, course: this.course, disableCache: true};
  }

  public getPending() {
    return this.pending;
  }

  public index() {
    return this.http.get(this.url('conversations'), this.getRequestOptions(this.getParams()))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError)
      .subscribe(c => {
        this.conversationsSubject.next(c);
      });
  }

  show(id: number) {
    return this.http.get(this.url(`conversations/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  messages(id: number, beforeId: number): Observable<Message[]> {
    return this.http.get(this.url(`conversations/${id}/messages`), this.getRequestOptions({before: beforeId || ''}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  readMessages(id: number) {
    this.http.post(this.url(`conversations/${id}/read`), {}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError)
      .subscribe(r => {
        Message.readConversation(id);
      })
  }

  send(id: number, text: string) {
    return this.http.post(this.url(`conversations/${id}`), {text}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  sendToUser(toUserId: number, text: string): Observable<Conversation> {
    return this.http.post(this.url('conversations'), {toUserId, text}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  sendToSolution(toSolutionId: number, text: string): Observable<Conversation> {
    return this.http.post(this.url(`solutions/${toSolutionId}/discuss`), {text}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  sendToProblem(toProblemId: number, text: string): Observable<Conversation> {
    return this.http.post(this.url(`problems/${toProblemId}/discuss`), {text}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  sendGeneral(text: string) {
    return this.http.post(this.url(`conversations/discuss`), {text}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
