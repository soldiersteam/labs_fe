import {Injectable} from '@angular/core';
import {Solution} from '../models/solution.model';
import {Observable} from 'rxjs';
import {BaseService} from './base.service';
import {Problem} from '../models/problem.model';
import {Status} from '../models/status.model';
import {ProgrammingLanguage} from '../models/programmingLanguage.model';

@Injectable()
export class SolutionService extends BaseService {

  protected makeModel(data: any) {
    return new Solution(data);
  }

  index(params: any): Observable<Solution[]> {
    const options = this.getRequestOptions();
    options.params = params;
    options.params['disableCache'] = true;
    return this.http.get(this.url(`solutions`), options)
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  indexFromModule(module_id: number): Observable<Solution[]> {
    return this.http.get(this.url(`solutions?module_id=${module_id}`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  forProblems(module_id: number, problems: Problem[]): Observable<Solution[]> {
    return this.http.get(this.url(`solutions?module_id=${module_id}&${problems.map(p => `problem_ids[]=${p.id}`).join('&')}`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  show(id: number, problem_id: number, module_id: number): Observable<Solution> {
    return this.http.get(this.url(`solutions/${id}`), this.getRequestOptions({
      module_id,
      problem_id,
      disableCache: true
    }))
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }

  reports(id: number) {
    return this.http.get(this.url(`solutions/${id}/reports`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  store(solution: Solution): Observable<Solution> {
    return this.http.post(this.url(`solutions`), {
      'course_id': solution.course.id,
      'module_id': solution.module.id,
      'problem_id': solution.problem.id,
      'programming_language_id': solution.programming_language.id,
      'solution_code': solution.code
    }, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
