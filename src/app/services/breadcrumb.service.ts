import {Injectable} from '@angular/core';
import {Breadcrumb} from '../models/breadcrumb.model';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class BreadcrumbService {

  private readonly crumbsSubject = new ReplaySubject<Breadcrumb[]>();
  public readonly update$ = this.crumbsSubject.asObservable();

  constructor() {
  }

  public update(breadcrumbs: Breadcrumb[]) {
    this.crumbsSubject.next(breadcrumbs);
  }

}
