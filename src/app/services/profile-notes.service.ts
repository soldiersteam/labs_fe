import {Injectable} from '@angular/core';
import {BaseCuratorService} from '../modules/curator-cabinet/services/base-curator.service';
import {UserNote} from '../models/user-note.model';

@Injectable()
export class ProfileNotesService extends BaseCuratorService {
  protected makeModel(data) {
    return new UserNote(data);
  }

  public index(userId: number) {
    return this.http.get(this.url(`users/${userId}/notes`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  public post(message: string, userId: number) {
    return this.http.post(this.url(`users/${userId}/notes`), {message}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  public remove(id: number) {
    return this.http.delete(this.url(`notes/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
