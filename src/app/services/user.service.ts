import {Injectable} from '@angular/core';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';
import {BaseService} from './base.service';
import {UserProfile} from '../models/user-profile.model';

@Injectable()
export class UserService extends BaseService {
  protected makeModel(data: any) {
    return new User(data);
  }

  public show(id: number): Observable<User> {
    return this.http.get(this.url(`users/${id}`), this.getRequestOptions({disableCache: true}))
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }

  public update(id: number, profile: UserProfile, image?: Blob): Observable<any> {
    const formData = new FormData();
    for (const el in profile) {
      if (profile.hasOwnProperty(el)) {
        formData.append(el, profile[el]);
      }
    }
    if (image) {
      formData.append('image', image);
    }
    return this.http.post(this.url(`users/${id}`), formData, this.getRequestOptions({}, false))
      .catch(this.handleError);
  }
}

