import {Injectable} from '@angular/core';
import {BaseService} from './base.service';

@Injectable()
export class RegisterStudentService extends BaseService {
  register(data: any) {
    return this.http.post(this.url('invite-parent'), data)
      .map(this.extractData)
      .catch(this.handleError)
  }
}
