import {Injectable} from '@angular/core';
import {Http, Response, RequestOptions, Headers} from '@angular/http';
import {Observable} from 'rxjs';
import {User} from '../models/user.model';
import {environment} from '../../environments/environment';
import {PageData} from '../models/page-data.model';
import {Router} from '@angular/router';
import {Language} from '../models/language.model';
import {NotificationsService} from 'angular2-notifications/dist';
import {isUndefined} from 'util';
import {Model} from '../models/model';

@Injectable()
export class BaseService {
  protected baseEndPoint = environment.api_url;
  public pageData: PageData = new PageData();

  public constructor(protected http: Http, protected router: Router, protected notificationsService: NotificationsService) {
    this.mapResourceData = this.mapResourceData.bind(this);
    this.handleError = this.handleError.bind(this);
  }

  protected makeModel(data): Model {
    return data;
  }

  protected extractData(res: Response) {
    return res.text() ? res.json() : {};
  }

  protected mapResourceData(json) {
    if (this.hasPager(json) || json.data) {
      this.pageData = new PageData(json.current_page, json.from, json.per_page, json.total, json.ended);
      return json.data.map(d => this.makeModel(d)) || {};
    }
    return json.map(d => this.makeModel(d)) || {};
  }

  protected handleError(errors: Response | any) {
    console.log(errors);
    if (errors.status !== 401 && errors.status !== 403 && errors.status !== 422) {
      this.notificationsService.error('Error!', (<string>errors));
    }
    if (errors.status === 401) {
      User.logout();
    } else if (errors.status === 403) {
      this.router.navigate(User.defaultRedirectTo());
    } else if (errors.json) {
      return Observable.throw(errors.json());
    }
    return Observable.of(errors);
  }

  protected url(action: string, params = null): string {
    return this.baseEndPoint + '/' + action;
  }

  protected hasPager(json) {
    return !isUndefined(json.current_page) &&
      !isUndefined(json.from) &&
      !isUndefined(json.per_page) &&
      !isUndefined(json.total)
  }

  protected getRequestOptions(params?: any, sendContentType = true): RequestOptions {
    const headers = new Headers();
    if (sendContentType) {
      headers.append('Content-Type', 'application/json');
    }
    headers.append('Accept-Language', Language.getCurrentLanguageCode());
    if (!User.guest()) {
      headers.append('Authorization', 'Bearer ' +
        User.getToken());
    }
    return new RequestOptions({
      headers: headers,
      ...{params}
    });
  }
}
