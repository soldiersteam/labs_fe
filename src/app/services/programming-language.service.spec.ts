import { TestBed, inject } from '@angular/core/testing';

import { ProgrammingLanguageService } from './programming-language.service';

describe('ProgrammingLanguageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProgrammingLanguageService]
    });
  });

  it('should be created', inject([ProgrammingLanguageService], (service: ProgrammingLanguageService) => {
    expect(service).toBeTruthy();
  }));
});
