import {Injectable} from '@angular/core';
import Echo from 'laravel-echo';
import {User} from '../models/user.model';
import {Message} from '../models/message.model';
import {environment} from '../../environments/environment';
import {Conversation} from '../models/conversation.model';
import {Solution} from '../models/solution.model';

@Injectable()
export class EchoService {

  protected echo;

  constructor() {
  }

  public init(user: User) {
    this.echo = new Echo({
      broadcaster: 'socket.io',
      host: environment.socket_url,
      path: '/socket.io',
      auth: {
        headers: {
          'Authorization': 'Bearer ' + user.token
        }
      }
    });
    this.echo.private(`user.${user.id}`)
      .listen('MessageSent', e => {
        if (e.message.user_id !== User.get().id) {
          Message.receivedMessages.push(e.message);
        }
        Message.received$.emit(e.message);
      })
      .listen('ConversationRead', e => Conversation.read$.emit({id: e.conversationId, userId: e.readById}))
      .listen('SolutionTested', e => Solution.tested$.emit(e.id));
  }
}
