import {Injectable} from '@angular/core';
import {BaseService} from './base.service';

@Injectable()
export class TranslationService extends BaseService {

  static translations = [];

  public translate(text: string): string {

    return TranslationService.translations[text] || `**${text}**`;
  }

  public index() {
    return this.http.get(this.url(`translations`), this.getRequestOptions())
        .map(this.extractData)
        .catch(this.handleError);
  }
}
