import {Injectable} from '@angular/core';
import {BaseService} from './base.service';

@Injectable()
export class MessageService extends BaseService {
  getUnread() {
    return this.http.get(this.url(`messages/unread`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
