import {Injectable} from '@angular/core';
import {BaseService} from './base.service';

@Injectable()
export class ProgrammingLanguageService extends BaseService {
  public show(id: number) {
    return this.http.get(this.url(`programming-languages/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  public index() {
    return this.http.get(this.url(`programming-languages`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  public search(str: string) {
    return this.http.get(this.url('curator/programming-languages/search'), this.getRequestOptions({query: str}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }
}
