import {Injectable} from '@angular/core';
import {RequestOptionsArgs, Response, Http, ResponseOptions, XHRBackend, RequestOptions} from '@angular/http';
import {Observable} from 'rxjs/Rx';

@Injectable()
export class InterceptedHttp extends Http {

  static cache = [];
  static cacheLiveTime = 100; // sec

  constructor(backend: XHRBackend, defaultOptions: RequestOptions) {
    super(backend, defaultOptions);
  }

  get(url: string, options?: RequestOptionsArgs): Observable<Response> {

    // const cacheDisabled = url.includes('solutions') || url.includes('conversations') || url.includes('users');
    const cacheDisabled = !(options.params && options.params['disableCache']);
    if (options.params) {
      delete options.params['disableCache'];
    }
    if (cacheDisabled && InterceptedHttp.cache[url] && InterceptedHttp.cache[url].expired_at > new Date()) {
      const responseOptions = new ResponseOptions({
        body: InterceptedHttp.cache[url].data
      });
      return Observable.of(new Response(responseOptions));
    }

    return super.get(url, options)
      .map(this.cacheData);
  }

  protected cacheData(res: Response) {
    InterceptedHttp.cache[res.url] = {
      expired_at: new Date(new Date().getTime() + InterceptedHttp.cacheLiveTime * 1000),
      data: res.text() ? res.json() : {}
    };
    return res;
  }
}
