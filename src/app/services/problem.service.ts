import {Injectable} from '@angular/core';
import {Problem} from '../models/problem.model';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';

@Injectable()
export class ProblemService extends BaseService {

  protected makeModel(data: any) {
    return new Problem(data);
  }

  index(module_id: number, with_latest_solution: boolean = false): Observable<Problem[]> {
    return this.http.get(this.url(`problems?module_id=${module_id}&with_latest_solution=${+with_latest_solution}`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  show(id: number, module_id: number): Observable<Problem> {
    return this.http.get(this.url(`problems/${id}?module_id=${module_id}`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }
}
