import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {Language} from '../models/language.model';
import {Observable} from 'rxjs';

@Injectable()
export class LanguageService extends BaseService {

  protected makeModel(data: any) {
    return new Language(data);
  }

  public index(): Observable<Language[]> {
    return this.http.get(this.url(`languages`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
