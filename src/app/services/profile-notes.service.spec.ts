import { TestBed, inject } from '@angular/core/testing';

import { ProfileNotesService } from './profile-notes.service';

describe('ProfileNotesService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [ProfileNotesService]
    });
  });

  it('should be created', inject([ProfileNotesService], (service: ProfileNotesService) => {
    expect(service).toBeTruthy();
  }));
});
