import { Injectable } from '@angular/core';
import {Module} from "../models/module.model";
import {BaseService} from './base.service';
import {Observable} from 'rxjs';

@Injectable()
export class ModuleService extends BaseService {

  protected makeModel(data: any) {
    return new Module(data);
  }

  index(course_id: number): Observable<Module[]> {
    return this.http.get(this.url(`modules`), this.getRequestOptions({course_id}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }
  show(id: number, with_latest_solution: boolean = false): Observable<Module> {
    return this.http.get(this.url(`modules/${id}`), this.getRequestOptions({with_latest_solution}))
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }

}
