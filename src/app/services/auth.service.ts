import {Injectable} from '@angular/core';
import {BaseService} from './base.service';
import {Observable} from 'rxjs';

@Injectable()
export class AuthService extends BaseService {

  public redirectOnLogin: string[] = null;

  public login(login: string, password: string): Observable<any> {
    return this.http.post(this.url('users/auth'), {login, password})
      .map(this.extractData)
      .catch(err => Observable.throw(err.json()));
  }

  public register(credentials): Observable<any> {
    return this.http.post(this.url('register'), credentials)
      .map(this.extractData)
      .catch(this.handleError);
  }

  public emailValidation(token: string): Observable<any> {

    return this.http.post(this.url('validate-email'), {
      token: token
    })
      .catch(this.handleError);
  }

  public resetPassword(credentials): Observable<any> {

    return this.http.post(this.url('password/reset'), credentials)
      .catch(this.handleError);
  }

  public sendResetToken(credentials): Observable<any> {

    return this.http.post(this.url('password/email'), credentials)
      .catch(this.handleError);
  }
}
