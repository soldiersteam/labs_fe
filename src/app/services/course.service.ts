import {Injectable} from '@angular/core';
import {Course} from '../models/course.model';
import {Observable} from 'rxjs';
import {BaseService} from './base.service';

@Injectable()
export class CourseService extends BaseService {

  protected makeModel(data: any): Course {
    return new Course(data);
  }

  index(): Observable<Course[]> {
    return this.http.get(this.url('courses'), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  show(id: number): Observable<Course> {
    return this.http.get(this.url(`courses/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }
}
