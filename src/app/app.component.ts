import {Component, ElementRef, OnInit, Renderer2} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';
import {User} from './models/user.model';
import {environment} from '../environments/environment';
import {EchoService} from './services/echo.service';
import {MessageService} from './services/message.service';
import {Message} from './models/message.model';
import {TranslationService} from './services/translation.service';
import {TranslatePipe} from './modules/shared/pipes/translate.pipe';
import {NotificationsService} from 'angular2-notifications/dist';
import {Language} from './models/language.model';
import * as moment from 'moment';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  public loaded = false;

  public notification_options = {
    position: ['bottom', 'right'],
    timeOut: 500000,
    theClass: 'custom-notification'
  };

  public constructor(protected router: Router,
                     protected renderer2: Renderer2,
                     protected el: ElementRef,
                     protected translationService: TranslationService,
                     protected echoService: EchoService,
                     protected messageService: MessageService,
                     protected notificationsService: NotificationsService,
                     protected translatePipe: TranslatePipe) {
    User.logout$.subscribe(() => this.router.navigate(User.defaultRedirectTo()));

  }

  ngOnInit() {
    this.loadMessagesLib();
    this.loadTranslations();
    moment.locale(Language.getCurrentLanguageCode());

    Message.received$.subscribe(m => {
      if (+m.user_id !== +User.get().id) {
        this.notificationsService.info(this.translatePipe.transform('You have a new message!'), m.message);
      }
    });

    this.router.events.subscribe((event: NavigationEnd) => {
      if (event instanceof NavigationEnd) {
        window.scrollTo(0, 0);
      }
    })
  }

  protected loadTranslations() {
    return this.translationService.index()
      .subscribe((data) => {
        TranslationService.translations = data;

        this.loaded = true; // @todo: refactor
      });
  }

  protected loadMessagesLib() {
    const s = this.renderer2.createElement('script');
    s.src = 'https://cdnjs.cloudflare.com/ajax/libs/socket.io/2.0.3/socket.io.js'; //   environment.socket_url + '/socket.io/socket.io.js';
    s.onload = () => {
      const loggedInUser = User.get();
      if (loggedInUser) {
        this.echoService.init(loggedInUser);
      }
      User.login$.subscribe((user: User) => this.echoService.init(user));
    };
    this.renderer2.appendChild(this.el.nativeElement, s);

    if (!User.guest()) {
      this.getUnread();
    }
    User.login$.subscribe(() => this.getUnread());
  }

  protected getUnread() {
    this.messageService.getUnread()
      .subscribe(m => {
        Message.receivedMessages = m;
        Message.readCountUpdate$.emit();
      });
  }
}
