import {NgModule} from '@angular/core';
import {MainComponent} from './components/main/main.component';
import {UserComponent} from './components/users/users.component';
import {SolutionsComponent} from './components/solutions/solutions.component';
import {CalendarComponent} from './components/calendar/calendar.component';
import {RouterModule} from '@angular/router';
import {curatorRoutes} from './curator-cabinet.routes';
import {UserService} from './services/user.service';
import {SolutionService} from './services/solution.service';
import {CoursesComponent} from './components/courses/courses.component';
import {CourseService} from './services/course.service';
import {ProblemsComponent} from './components/problems/problems.component';
import {ProblemService} from './services/problem.service';
import {CalendarService} from './services/calendar.service';
import {UserCourseContainerComponent} from './components/user-course-container/user-course-container.component';
import {Http} from '@angular/http';
import {InterceptedHttp} from '../../services/interceptedHttp';
import {SharedModule} from '../shared/shared.module';
import {CuratorConversationsComponent} from './components/curator-conversations/curator-conversations.component';
import {SolutionDropdownComponent} from './components/solution-dropdown/solution-dropdown.component';
import {CommentService} from './services/comment.service';
import {SolutionGroupService} from './services/solution-group.service';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(curatorRoutes),
  ],
  declarations: [
    MainComponent,
    UserComponent,
    SolutionsComponent,
    CalendarComponent,
    CoursesComponent,
    ProblemsComponent,
    UserCourseContainerComponent,
    CuratorConversationsComponent,
    SolutionDropdownComponent,
  ],
  providers: [
    UserService,
    SolutionService,
    CourseService,
    ProblemService,
    CalendarService,
    CommentService,
    SolutionGroupService,
    {
      provide: Http,
      useClass: InterceptedHttp
    }
  ]
})
export class CuratorCabinetModule {
}
