import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';

@Injectable()
export class ProblemService extends BaseCuratorService {
  index() {
    return this.http.get(this.url('problems'), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  show(id: number) {
    return this.http.get(this.url(`problems/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
