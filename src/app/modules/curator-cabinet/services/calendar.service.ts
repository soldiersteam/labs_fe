import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';
import {Observable} from 'rxjs/Observable';
import {Interview} from '../../../models/interview.model';

@Injectable()
export class CalendarService extends BaseCuratorService {
  protected makeModel(data: any) {
    return new Interview(data);
  }

  public getInterviews(): Observable<Interview[]> {
    return this.http.get(this.url('interviews'), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  public complete(id: number) {
    return this.http.post(this.url(`interviews/${id}/complete`), {}, this.getRequestOptions())
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }
}
