import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';
import {SolutionGroup} from '../../../models/solution-group.model';
import {Comment} from '../../../models/comment.model';

@Injectable()
export class CommentService extends BaseCuratorService {
  makeModel(data) {
    return new Comment(data);
  }

  sendToSolutionGroup(group: SolutionGroup, text: string) {
    return this.http.post(this.url(`solutions/comment`), {
      text,
      ...group.filterAttributes
    }, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
