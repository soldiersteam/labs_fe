import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';
import {Model} from '../../../models/model';
import {SolutionGroup} from '../../../models/solution-group.model';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class SolutionGroupService extends BaseCuratorService {


  protected makeModel(data): Model {
    return new SolutionGroup(data);
  }

  index(page: number, filters: any): Observable<SolutionGroup[]> {
    return this.http.get(this.url('solution-groups'), this.getRequestOptions({page, ...filters, disableCache: true}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }
}
