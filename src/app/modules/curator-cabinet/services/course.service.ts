import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';

@Injectable()
export class CourseService extends BaseCuratorService {
  public index(studentId?: number) {
    return this.http.get(this.url('courses'), this.getRequestOptions(studentId ? {student_id: studentId} : {}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  public search(str: string) {
    return this.http.get(this.url('courses/search'), this.getRequestOptions({query: str}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }
}
