import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';

@Injectable()
export class UserService extends BaseCuratorService {
  index() {
    return this.http.get(this.url('users'), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  show(id: number) {
    return this.http.get(this.url(`users/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  giveAccess(id: number, course_ids: number[], success_url: string) {
    return this.http.post(this.url(`users/${id}/give-access`), {course_ids, success_url}, this.getRequestOptions())
      .map(this.extractData)
      .map(this.handleError);
  }

  public search(str: string) {
    return this.http.get(this.url('users/search'), this.getRequestOptions({query: str}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }
}
