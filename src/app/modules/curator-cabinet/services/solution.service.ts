import {Injectable} from '@angular/core';
import {BaseCuratorService} from './base-curator.service';
import {Model} from '../../../models/model';
import {SolutionGroup} from '../../../models/solution-group.model';
import {Observable} from 'rxjs/Observable';
import {Solution} from '../../../models/solution.model';

@Injectable()
export class SolutionService extends BaseCuratorService {


  protected makeModel(data): Model {
    return new Solution(data);
  }

  indexForGroup(group: SolutionGroup): Observable<Solution[]> {
    return this.http.get(this.url('solutions'), this.getRequestOptions({...group.filterAttributes, disableCache: true}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }
}
