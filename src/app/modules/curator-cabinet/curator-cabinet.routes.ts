import {Routes} from '@angular/router';
import {UserComponent} from './components/users/users.component';
import {CalendarComponent} from './components/calendar/calendar.component';
import {SolutionsComponent} from './components/solutions/solutions.component';
import {CoursesComponent} from './components/courses/courses.component';
import {ProblemsComponent} from './components/problems/problems.component';
import {CourseComponent} from '../shared/components/course/course.component';
import {ModuleComponent} from '../shared/components/module/module.component';
import {ProblemComponent} from '../shared/components/problem/problem.component';
import {SolutionComponent} from '../shared/components/solution/solution.component';
import {ConversationComponent} from '../shared/components/conversation/conversation.component';
import {CuratorConversationsComponent} from './components/curator-conversations/curator-conversations.component';

/**
 * Created by dima on 6/22/17.
 */
export const curatorRoutes: Routes = [
  {
    path: '',
    children: [
    // {path: '', component: MainComponent},
    {path: '', redirectTo: 'solutions', pathMatch: 'full'},
    {path: 'solutions', component: SolutionsComponent},
    {path: 'users', component: UserComponent},
    {path: 'problems', component: ProblemsComponent},
    {path: 'calendar', component: CalendarComponent},
    {path: 'courses', component: CoursesComponent},
    {
      path: 'messages',
      children: [
        {
          path: '',
          component: CuratorConversationsComponent
        },
        {
          path: ':conversation_id',
          component: ConversationComponent
        }
      ]
    },
    {
      path: 'courses/:course_id',
      children: [
        {
          path: '',
          component: CourseComponent,
          data: {title: 'Course'}
        },
        {
          path: 'modules/:module_id',
          children: [
            {
              path: '',
              component: ModuleComponent,
              data: {title: 'Module'},
            },
            {
              path: 'problems/:problem_id',
              children: [
                {
                  path: '',
                  component: ProblemComponent,
                  data: {title: 'Problem'},
                },
                {
                  path: 'solutions/:solution_id',
                  component: SolutionComponent,
                  data: {title: 'Solution'}
                }
              ]
            },
          ]
        }
      ]
    }
  ]
  }
];
