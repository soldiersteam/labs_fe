import {Component, OnInit} from '@angular/core';
import {UserService} from '../../services/user.service';
import {User} from '../../../../models/user.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SendMessageModalComponent} from '../../../shared/components/send-message-modal/send-message-modal.component';

@Component({
  selector: 'app-user',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UserComponent implements OnInit {

  public users: User[] = [];

  constructor(protected userService: UserService, protected modal: NgbModal) {
  }

  ngOnInit() {
    // todo paginate
    this.userService.index().subscribe(u => this.users = u);
  }

  sendMessage(userId: number) {
    this.modal.open(SendMessageModalComponent).componentInstance.userId = userId;
  }
}
