import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Course} from '../../../../models/course.model';
import {User} from '../../../../models/user.model';
import {UserService} from '../../services/user.service';
import {Router} from '@angular/router';
import {environment} from '../../../../../environments/environment';

@Component({
  selector: 'app-user-course-container',
  templateUrl: './user-course-container.component.html',
  styleUrls: ['./user-course-container.component.css']
})
export class UserCourseContainerComponent implements OnInit {

  @Input() courses: Course[];
  @Output() coursesChange = new EventEmitter();
  @Input() user: User = new User;

  constructor(protected userService: UserService, protected router: Router) {
  }

  ngOnInit() {
  }

  giveAccess() {
    this.userService.giveAccess(this.user.id, this.courses.map(course => course.id), environment.site_url + '/parent/courses')
      .subscribe(r => this.router.navigate(User.defaultRedirectTo()));
  }

  remove(course: Course) {
    this.courses.splice(this.courses.indexOf(course, 1));
    this.coursesChange.emit(this.courses);
  }
}
