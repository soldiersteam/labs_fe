import {Component, OnInit} from '@angular/core';
import {ConversationService} from '../../../../services/conversation.service';
import {SelectItem} from '../../../../models/select-item.model';
import {CourseService} from '../../services/course.service';
import {Course} from '../../../../models/course.model';

@Component({
  selector: 'app-curator-conversations',
  templateUrl: './curator-conversations.component.html',
  styleUrls: ['./curator-conversations.component.css']
})
export class CuratorConversationsComponent implements OnInit {

  public courseSearchItems: Course[] = [];

  public pending = false;
  public loading = true;

  constructor(private conversationService: ConversationService, private courseService: CourseService) {
  }

  ngOnInit() {
    this.conversationService.conversationsObservable.subscribe(r => this.loading = false);
  }

  public courseType(search: string) {
    if (search) {
      this.courseService.search(search)
        .subscribe(c => this.courseSearchItems = c.map(course => new SelectItem(course.id, course.name)));
    } else {
      this.courseSearchItems = []
    }
  }

  public courseChange(items: SelectItem[]) {
    this.conversationService.courseSubject.next(items.length ? items[0].id : undefined);
  }

  pendingChange() {
    this.loading = true;
    this.conversationService.pendingSubject.next(this.pending);
  }
}
