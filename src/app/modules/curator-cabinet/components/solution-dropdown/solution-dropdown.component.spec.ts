import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SolutionDropdownComponent } from './solution-dropdown.component';

describe('SolutionDropdownComponent', () => {
  let component: SolutionDropdownComponent;
  let fixture: ComponentFixture<SolutionDropdownComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SolutionDropdownComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SolutionDropdownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
