import {Component, Input, OnInit} from '@angular/core';
import {SolutionGroup} from '../../../../models/solution-group.model';

@Component({
  selector: 'app-solution-dropdown',
  templateUrl: './solution-dropdown.component.html',
  styleUrls: ['./solution-dropdown.component.css']
})
export class SolutionDropdownComponent implements OnInit {

  @Input() group: SolutionGroup;

  constructor() {
  }

  ngOnInit() {
  }

}
