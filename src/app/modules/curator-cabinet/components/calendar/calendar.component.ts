import {Component, OnInit} from '@angular/core';
import {CalendarService} from '../../services/calendar.service';
import {Interview} from '../../../../models/interview.model';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {

  public interviews: Interview[] = [];

  constructor(protected calendarService: CalendarService, protected userService: UserService) {
  }

  ngOnInit() {
    this.calendarService.getInterviews()
      .subscribe(interviews => {
        this.interviews = interviews;
        for (const i of interviews) {
          this.userService.show(i.student_id).subscribe(u => i.student = u);
        }
      });
  }

  public complete(interview: Interview) {
    this.interviews.splice(this.interviews.indexOf(interview), 1);
    this.calendarService.complete(interview.id).subscribe();
  }
}
