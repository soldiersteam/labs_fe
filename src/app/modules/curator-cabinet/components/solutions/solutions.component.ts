import {Component, OnInit} from '@angular/core';
import {SolutionService} from '../../services/solution.service';
import {Problem} from '../../../../models/problem.model';
import {User} from '../../../../models/user.model';
import {UserService} from '../../services/user.service';
import {Course} from '../../../../models/course.model';
import {ProgrammingLanguage} from '../../../../models/programmingLanguage.model';
import {CourseService} from '../../services/course.service';
import {SelectItem} from '../../../../models/select-item.model';
import {ProgrammingLanguageService} from '../../../../services/programming-language.service';
import {SortItem} from '../../../../models/sort-item.model';
import {TranslatePipe} from '../../../shared/pipes/translate.pipe';
import {SolutionGroup} from '../../../../models/solution-group.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SendMessageModalComponent} from '../../../shared/components/send-message-modal/send-message-modal.component';
import {SolutionGroupService} from '../../services/solution-group.service';

@Component({
  selector: 'app-solution',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.css']
})
export class SolutionsComponent implements OnInit {

  public solutionGroups: SolutionGroup[] = [];

  public courseSearchItems: Course[] = [];
  public studentSearchItems: User[] = [];
  public languageSearchItems: ProgrammingLanguage[] = [];

  public readonly headItems = [
    new SortItem(this.translatePipe.transform('Problem'), 'problem'),
    new SortItem(this.translatePipe.transform('Student'), 'user'),
    new SortItem(this.translatePipe.transform('Programming language'), 'programming_language'),
    new SortItem(this.translatePipe.transform('Course'), 'course'),
    new SortItem(this.translatePipe.transform('Comment'), 'comment_id'),
  ];

  protected filterCourses: SelectItem[] = [];
  protected filterStudents: SelectItem[] = [];
  protected filterLanguages: SelectItem[] = [];
  protected sort: SortItem;

  constructor(public solutionGroupService: SolutionGroupService,
              private solutionService: SolutionService,
              protected courseService: CourseService,
              protected userService: UserService,
              protected programmingLanguageService: ProgrammingLanguageService,
              protected translatePipe: TranslatePipe,
              private modal: NgbModal) {
  }

  ngOnInit() {
    this.getData();
  }

  public courseType(search: string) {
    if (search) {
      this.courseService.search(search)
        .subscribe(c => this.courseSearchItems = c.map(course => new SelectItem(course.id, course.name)));
    } else {
      this.courseSearchItems = []
    }
  }

  public courseChange(items: SelectItem[]) {
    this.filterCourses = items;
    this.getData(1)
  }

  public languageType(search: string) {
    if (search) {
      this.programmingLanguageService.search(search)
        .subscribe(l => this.languageSearchItems = l.map(language => new SelectItem(language.id, language.title)));
    } else {
      this.languageSearchItems = []
    }
  }

  public languageChange(items: SelectItem[]) {
    this.filterLanguages = items;
    this.getData(1)
  }

  public userType(search: string) {
    if (search) {
      this.userService.search(search)
        .subscribe(u => this.studentSearchItems = u.map(user => new SelectItem(user.id, user.name)));
    } else {
      this.studentSearchItems = []
    }
  }

  public sortBy(sort: SortItem) {
    this.sort = sort;
    this.getData();
  }

  public userChange(items: SelectItem[]) {
    this.filterStudents = items;
    this.getData(1)
  }

  protected getRequestData() {
    return {
      'courses[]': this.filterCourses.map(i => i.id),
      'programmingLanguages[]': this.filterLanguages.map(i => i.id),
      'students[]': this.filterStudents.map(i => i.id),
      ...(this.sort ? this.sort.getRequestData() : {})
    };
  }

  public pageChange(page) {
    this.getData(page);
  }

  public comment(group) {
    const modal = this.modal.open(SendMessageModalComponent);
    modal.componentInstance.solutionGroup = group;
    modal.componentInstance.success.subscribe(comment => {
      group.comment = comment;
    });
  }

  public open(group: SolutionGroup) {
    group.open = !group.open;
    if (!group.solutions.length && group.open) {
      this.solutionService.indexForGroup(group).subscribe(solutions => {
        group.solutions = solutions;
      });
    }
  }

  protected getData(page = 1) {
    if (Number.isSafeInteger(page)) {
      this.solutionGroupService.index(page, this.getRequestData())
        .subscribe(s => this.solutionGroups = s);
    }
  }

}
