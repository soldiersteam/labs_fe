import {Component, OnInit} from '@angular/core';
import {CourseService} from '../../services/course.service';
import {Course} from '../../../../models/course.model';
import {User} from '../../../../models/user.model';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../services/user.service';

@Component({
  selector: 'app-course',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CoursesComponent implements OnInit {

  public courses: Course[] = [];
  public student: User;
  public selectedCourses: Course[] = [];

  constructor(public courseService: CourseService, protected route: ActivatedRoute, protected userService: UserService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(p => {
      if (p.studentId) {
        this.userService.show(p.studentId)
          .subscribe(s => this.student = s);
      }
      this.courseService.index(p.studentId).subscribe(c => this.courses = c);
    });

  }

  giveAccess(course: Course) {
    if (this.selectedCourses.indexOf(course) === -1) {
      this.selectedCourses.push(course);
    }
  }
}
