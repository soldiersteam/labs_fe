import {Component, OnInit} from '@angular/core';
import {ProblemService} from '../../services/problem.service';
import {Problem} from '../../../../models/problem.model';

@Component({
  selector: 'app-problem',
  templateUrl: './problems.component.html',
  styleUrls: ['./problems.component.css']
})
export class ProblemsComponent implements OnInit {

  public problems: Problem[] = [];

  constructor(protected problemService: ProblemService) {
  }

  ngOnInit() {
    this.problemService.index().subscribe(p => this.problems = p);
  }

}
