/**
 * Created by dima on 7/2/17.
 */
import {Routes} from '@angular/router';
import {BreadcrumbGuard} from '../../guards/breadcrumb.guard';
import {MainComponent} from './components/main/main.component';
import {CourseComponent} from '../shared/components/course/course.component';
import {ModuleComponent} from '../shared/components/module/module.component';
import {ProblemComponent} from '../shared/components/problem/problem.component';
import {SolutionComponent} from '../shared/components/solution/solution.component';
import {OwnedCoursesComponent} from './components/owned-courses/owned-courses.component';
import {ConversationComponent} from '../shared/components/conversation/conversation.component';
import {ConversationsComponent} from '../shared/components/conversations/conversations.component';
import {SolutionsComponent} from './components/solutions/solutions.component';

export const studentRoutes: Routes = [
  {
    path: '',
    canActivateChild: [BreadcrumbGuard],
    children: [
      {
        path: '',
        component: MainComponent,
        data: {title: 'Home'}
      },
      {
        path: 'owned-courses',
        component: OwnedCoursesComponent
      },
      {
        path: 'solutions',
        component: SolutionsComponent
      },
      {
        path: 'messages',
        children: [
          {
            path: '',
            component: ConversationsComponent
          },
          {
            path: ':conversation_id',
            component: ConversationComponent
          }
        ]
      },
      {
        path: 'courses/:course_id',
        children: [
          {
            path: '',
            component: CourseComponent,
            data: {title: 'Course'}
          },
          {
            path: 'modules/:module_id',
            children: [
              {
                path: '',
                component: ModuleComponent,
                data: {title: 'Module'},
              },
              {
                path: 'problems/:problem_id',
                children: [
                  {
                    path: '',
                    component: ProblemComponent,
                    data: {title: 'Problem'},
                  },
                  {
                    path: 'solutions/:solution_id',
                    component: SolutionComponent,
                    data: {title: 'Solution'}
                  }
                ]
              },
            ]
          }
        ]
      }
    ]
  },
  // { path: '**', component: PageNotFoundComponent }
];
