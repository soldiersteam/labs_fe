import {NgModule} from '@angular/core';
import {MainComponent} from './components/main/main.component';
import {RouterModule} from '@angular/router';
import {studentRoutes} from './student.routes';
import {SharedModule} from '../shared/shared.module';
import {OwnedCoursesComponent} from './components/owned-courses/owned-courses.component';
import {CourseService} from './services/course.service';
import {InterceptedHttp} from '../../services/interceptedHttp';
import {Http} from '@angular/http';
import {SolutionsComponent} from './components/solutions/solutions.component';

@NgModule({
  imports: [
    SharedModule,
    RouterModule.forChild(studentRoutes),
  ],
  declarations: [
    MainComponent,
    OwnedCoursesComponent,
    SolutionsComponent,
  ],
  providers: [
    CourseService,
    {
      provide: Http,
      useClass: InterceptedHttp
    }
  ]
})
export class StudentModule {
}
