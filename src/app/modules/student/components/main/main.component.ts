import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {User} from '../../../../models/user.model';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  public noCourse = false;

  constructor(protected router: Router) {
  }

  ngOnInit() {
    const courseId = User.get().course_id;
    if (courseId) {
      this.router.navigate(['/student', 'courses', courseId]);
    } else {
      this.noCourse = true;
    }
  }
}
