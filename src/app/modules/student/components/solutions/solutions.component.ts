import {Component, OnInit} from '@angular/core';
import {SolutionService} from '../../../../services/solution.service';
import {Solution} from '../../../../models/solution.model';

@Component({
  selector: 'app-solutions',
  templateUrl: './solutions.component.html',
  styleUrls: ['./solutions.component.css']
})
export class SolutionsComponent implements OnInit {

  solutions: Solution[] = [];

  constructor(public solutionService: SolutionService) {
  }


  ngOnInit() {
    this.getPreviousSolutions();
  }

  getPreviousSolutions(page = 1) {
    if (page) {
      this.solutionService.index({
        with_problems: true,
        page: page
      })
        .subscribe(solutions => {
          this.solutions = solutions;
        });
    }
  }

  pageChange(page) {
    this.getPreviousSolutions(page);
  }
}
