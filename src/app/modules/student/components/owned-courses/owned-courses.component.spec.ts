import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OwnedCoursesComponent } from './owned-courses.component';

describe('OwnedCoursesComponent', () => {
  let component: OwnedCoursesComponent;
  let fixture: ComponentFixture<OwnedCoursesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OwnedCoursesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OwnedCoursesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
