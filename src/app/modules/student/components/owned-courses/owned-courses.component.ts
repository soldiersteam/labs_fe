import {Component, OnInit} from '@angular/core';
import {CourseService} from '../../services/course.service';
import {Course} from '../../../../models/course.model';

@Component({
  selector: 'app-owned-courses',
  templateUrl: './owned-courses.component.html',
  styleUrls: ['./owned-courses.component.css']
})
export class OwnedCoursesComponent implements OnInit {

  public courses: Course[] = [];

  constructor(protected courseService: CourseService) {
  }

  ngOnInit() {
    this.courseService.getOwned().subscribe(c => this.courses = c);
  }

}
