import {Injectable} from '@angular/core';
import {BaseStudentService} from './base-student.service';

@Injectable()
export class CourseService extends BaseStudentService {
  public getOwned() {
    return this.http.get(this.url('owned-courses'), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError)
  }
}
