import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ConversationService} from '../../../../services/conversation.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute, Router} from '@angular/router';
import {Conversation} from '../../../../models/conversation.model';
import {SolutionGroup} from '../../../../models/solution-group.model';
import {User} from '../../../../models/user.model';
import {CommentService} from '../../../curator-cabinet/services/comment.service';

@Component({
  selector: 'app-send-message-modal',
  templateUrl: './send-message-modal.component.html',
  styleUrls: ['./send-message-modal.component.css']
})
export class SendMessageModalComponent implements OnInit {

  @Input() userId: number;
  @Input() solutionId: number;
  @Input() problemId: number;
  @Input() solutionGroup: SolutionGroup;

  @Output() readonly success = new EventEmitter<boolean>();

  constructor(protected conversationService: ConversationService,
              protected commentService: CommentService,
              protected modal: NgbActiveModal,
              protected router: Router,
              protected route: ActivatedRoute) {
    this.handle = this.handle.bind(this);
  }

  ngOnInit() {
  }

  protected handle(conv: Conversation) {
    this.modal.close();
    this.router.navigate(['/', this.router.url.split('/')[1], 'messages', conv.id]);
  }

  sendMessage(message: string) {
    if (this.solutionGroup) {
      this.commentService.sendToSolutionGroup(this.solutionGroup, message).subscribe(comment => {
        this.modal.close();
        this.success.emit(comment);
      });
    } else if (this.userId) {
      this.conversationService.sendToUser(this.userId, message).subscribe(this.handle);
    } else if (this.solutionId) {
      this.conversationService.sendToSolution(this.solutionId, message).subscribe(this.handle);
    } else if (this.problemId) {
      this.conversationService.sendToProblem(this.problemId, message).subscribe(this.handle);
    } else {
      this.conversationService.sendGeneral(message).subscribe(this.handle);
    }
  }
}
