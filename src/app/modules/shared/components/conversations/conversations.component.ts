import {Component, Input, OnInit} from '@angular/core';
import {ConversationService} from '../../../../services/conversation.service';
import {Conversation} from '../../../../models/conversation.model';

@Component({
  selector: 'app-conversations',
  templateUrl: './conversations.component.html',
  styleUrls: ['./conversations.component.css']
})
export class ConversationsComponent implements OnInit {

  @Input() public pending = false;
  public conversations: Conversation[];

  constructor(protected conversationService: ConversationService) {
  }

  ngOnInit() {
    this.conversationService.conversationsObservable.subscribe(c => this.conversations = c);
    this.conversationService.pendingSubject.next(this.pending);
  }
}
