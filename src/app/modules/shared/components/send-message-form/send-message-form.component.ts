import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-send-message-form',
  templateUrl: './send-message-form.component.html',
  styleUrls: ['./send-message-form.component.css']
})
export class SendMessageFormComponent implements OnInit {

  @Output() readonly sent = new EventEmitter<string>();
  @Input() messageText = '';
  @Input() disabled = false;
  @Output() readonly messageTextChange = new EventEmitter<string>();

  protected shiftPressed = false;

  constructor() {
  }

  ngOnInit() {
  }

  keyDownFunction(event) {
    if (event.keyCode === 16) {
      this.shiftPressed = true;
    }

    if (event.keyCode === 13 && !this.shiftPressed && !this.disabled) {
      this.sent.emit(this.messageText);
    }
  }

  keyUpFunction(event) {
    if (this.messageText.trim().length === 0) {
      this.messageText = '';
    }
    if (event.keyCode === 16) {
      this.shiftPressed = false;
    }
  }
}
