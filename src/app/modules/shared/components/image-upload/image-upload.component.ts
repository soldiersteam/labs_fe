import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {CropperSettings} from 'ng2-img-cropper';

@Component({
  selector: 'app-image-upload',
  templateUrl: './image-upload.component.html',
  styleUrls: ['./image-upload.component.css']
})
export class ImageUploadComponent implements OnInit {

  public readonly cropperSettings = new CropperSettings();
  public data: any = {};
  @Output() public readonly imageChange = new EventEmitter<Blob>();

  constructor() {

    this.cropperSettings.croppedWidth = 150;
    this.cropperSettings.croppedHeight = 150;
    this.cropperSettings.canvasWidth = 400;
    this.cropperSettings.canvasHeight = 400;
    this.cropperSettings.cropperClass = 'hidden';
    this.cropperSettings.croppingClass = 'visible'
  }

  ngOnInit() {
  }

  change() {
    // convert base64 to raw binary data held in a string
    // doesn't handle URLEncoded DataURIs - see SO answer #6850276 for code that does this
    const byteString = atob(this.data.image.split(',')[1]);

    // separate out the mime component
    const mimeString = this.data.image.split(',')[0].split(':')[1].split(';')[0];

    // write the bytes of the string to an ArrayBuffer
    const ab = new ArrayBuffer(byteString.length);

    // create a view into the buffer
    const ia = new Uint8Array(ab);

    // set the bytes of the buffer to the correct values
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    // write the ArrayBuffer to a blob, and you're done
   this.imageChange.emit(new Blob([ab], {type: mimeString}));
  }
}
