import {Component, ElementRef, OnDestroy, OnInit, ViewChild, AfterViewChecked} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {ConversationService} from '../../../../services/conversation.service';
import {Message} from '../../../../models/message.model';
import {User} from '../../../../models/user.model';
import {Subscriber} from 'rxjs/Subscriber';
import {Conversation} from '../../../../models/conversation.model';

@Component({
  selector: 'app-conversation',
  templateUrl: './conversation.component.html',
  styleUrls: ['./conversation.component.css']
})
export class ConversationComponent implements OnInit, OnDestroy, AfterViewChecked {

  public isSendButtonDisabled = false;

  public messages: Message[] = [];
  public messageText = '';
  public conversationId: number;
  public loading = false;
  protected messageSubscriber: Subscriber<Message>;
  protected conversationSubscriber: Subscriber<number>;
  private readonly loadScrollWhenLessThan = 15;
  private prevHeight: number;
  private scrollDown = true;
  private needScroll = true;
  private ended;
  @ViewChild('dialogContainer') container: ElementRef;

  constructor(protected activatedRoute: ActivatedRoute,
              protected conversationService: ConversationService) {
  }

  ngOnInit() {
    this.conversationId = +this.activatedRoute.snapshot.params['conversation_id'];
    this.getMessages(true);

    this.messageSubscriber = Message.received$.subscribe(m => {
      if (+m.conversation_id === this.conversationId && +m.user_id !== +User.get().id) {
        if (this.messages.length && this.messages[this.messages.length - 1].user_id !== m.user_id) {
          m.show_user = true;
        }
        this.messages.push(m);
        this.scrollDown = true;
        this.needScroll = true;
        this.markAsRead();
      }
    });

    this.conversationSubscriber = Conversation.read$.subscribe(data => {
      if (this.conversationId === +data.id) {
        for (const message of this.messages) {
          if (message.user_id !== +data.userId) {
            message.is_read = true;
          }
        }
      }
    });
  }

  ngAfterViewChecked() {
    if (this.needScroll) {
      if (this.scrollDown) {
        this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight;
      } else {
        this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight - this.prevHeight;
      }
      this.needScroll = false;
    }
  }

  ngOnDestroy() {
    this.messageSubscriber.unsubscribe();
    this.conversationSubscriber.unsubscribe();
  }

  markAsRead() {
    this.conversationService.readMessages(this.conversationId);
  }

  getMessages(firstLoad = false) {
    if (!this.loading) {
      this.prevHeight = this.container.nativeElement.scrollHeight;
      this.scrollDown = firstLoad;
      this.loading = true;
      this.conversationService.messages(this.conversationId, this.messages.length ? this.messages[0].id : null).subscribe(m => {
        this.ended = this.conversationService.pageData.ended;
        this.needScroll = true;
        this.messages = [...m.reverse(), ...this.messages];
        this.loading = false;
        this.setShowUser();
        this.markAsRead();
      })
    }

  }

  sendMessage() {
    if (this.messageText.trim()) {
      this.isSendButtonDisabled = true;
      this.conversationService.send(this.conversationId, this.messageText)
        .subscribe((m: Message) => {
          if (this.messages.length && this.messages[this.messages.length - 1].user_id !== m.user_id) {
            m.show_user = true;
          }
          this.messages.push(m);
          this.messageText = '';
          this.isSendButtonDisabled = false;

          setTimeout(() => this.container.nativeElement.scrollTop = this.container.nativeElement.scrollHeight, 100);
        });
    }
  }


  protected setShowUser() {
    if (this.messages.length > 1) {
      let prev = this.messages[0];
      prev.show_user = true;
      for (let i = 1; i < this.messages.length; ++i) {
        this.messages[i].show_user = !(this.messages[i].user_id === prev.user_id);
        prev = this.messages[i];
      }
    }
  }

  scrolled(e: Event) {
    if ((<HTMLDivElement>e.target).scrollTop < this.loadScrollWhenLessThan && !this.ended) {
      this.getMessages();
    }
  }
}
