import {Component, ElementRef, EventEmitter, HostListener, Input, OnInit, Output} from '@angular/core';
import {SelectItem} from '../../../../models/select-item.model';
import {AnonymousSubject, Subject} from 'rxjs/Subject';

@Component({
  selector: 'app-multi-select',
  templateUrl: './multi-select.component.html',
  styleUrls: ['./multi-select.component.css'],
})
export class MultiSelectComponent implements OnInit {

  @Input() public items: SelectItem[];
  @Input() public placeholder = '';
  @Input() public multiple = true;

  @Output() public searchStringChange: any = new EventEmitter<string>()
    .debounceTime(500)
    .distinctUntilChanged();

  @Output() public selectChange = new EventEmitter<SelectItem[]>();

  public selectedItems: SelectItem[] = [];
  public open = false;
  public searchString = '';

  constructor(protected eRef: ElementRef) {
  }

  ngOnInit() {
  }

  public select(item: SelectItem) {
    if (!this.selectedItems.some(i => i.id === item.id)) {
      if (this.multiple) {
        this.selectedItems.push(item);
      } else {
        this.selectedItems = [item];
      }
      this.selectChange.emit(this.selectedItems);
    }
  }

  @HostListener('document:click', ['$event'])
  public click(event: Event) {
    if (!this.eRef.nativeElement.contains(event.target)) {
      this.open = false;
    }
  }

  public deselect(item: SelectItem) {
    this.selectedItems = this.selectedItems.filter(i => i.id !== item.id);
    this.selectChange.emit(this.selectedItems);
  }
}
