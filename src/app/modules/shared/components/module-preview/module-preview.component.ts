import {Component, OnInit, Input} from '@angular/core';
import {Module} from '../../../../models/module.model';

@Component({
  selector: 'app-module-preview',
  templateUrl: './module-preview.component.html',
  styleUrls: ['./module-preview.component.css']
})
export class ModulePreviewComponent implements OnInit {

  @Input() module: Module;

  constructor() {
  }

  ngOnInit() {
  }

}
