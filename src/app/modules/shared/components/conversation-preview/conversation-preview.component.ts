import {Component, Input, OnInit} from '@angular/core';
import {Conversation} from '../../../../models/conversation.model';
import {User} from '../../../../models/user.model';
import {Message} from '../../../../models/message.model';
import {TranslatePipe} from '../../pipes/translate.pipe';

@Component({
  selector: 'app-conversation-preview',
  templateUrl: './conversation-preview.component.html',
  styleUrls: ['./conversation-preview.component.css']
})
export class ConversationPreviewComponent implements OnInit {

  @Input() public conversation: Conversation;

  public loaded = false;

  public picture = null;
  public header = '';
  public lastMessage: Message = null;

  constructor(protected translatePipe: TranslatePipe) {
  }

  ngOnInit() {
    const toUser = this.conversation.users.find(user => user.id !== User.id());
    if (toUser) {
      this.picture = toUser.avatar;
    } else {
      this.picture = User.noAvatarPicture;
    }

    Message.received$.subscribe((message: Message) => {
      if (message.conversation_id === this.conversation.id) {
        this.lastMessage = message;
      }
    });

    switch (this.conversation.topic_type) {
      case 'solutions':
        this.header = this.translatePipe.transform('Solution') + ` #${this.conversation.topic_id}`;
        break;
      case 'problems':
        this.header = this.translatePipe.transform('Problem') + ': ' + this.conversation.name;
        break;
      default:
        this.header = this.translatePipe.transform('Assistance');
    }

    this.lastMessage = this.conversation.messages[0];
    this.lastMessage.user = this.conversation.users.find(user => user.id === this.conversation.messages[0].user_id);

    this.loaded = true;
  }

}
