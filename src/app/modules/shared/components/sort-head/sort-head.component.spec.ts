import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SortHeadComponent } from './sort-head.component';

describe('SortHeadComponent', () => {
  let component: SortHeadComponent;
  let fixture: ComponentFixture<SortHeadComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SortHeadComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SortHeadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
