import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SortItem} from '../../../../models/sort-item.model';

@Component({
  selector: '[app-sort-head]',
  templateUrl: './sort-head.component.html',
  styleUrls: ['./sort-head.component.css']
})
export class SortHeadComponent implements OnInit {

  @Input() public items: SortItem[];
  @Output() public sort: any = new EventEmitter<SortItem>()
    .debounceTime(500);

  constructor() {
  }

  ngOnInit() {
  }

  public toggle(item: SortItem) {
    for (const i of this.items) {
      if (i === item) {
        i.next();
        this.sort.next(i);
      } else {
        i.reset();
      }
    }
  }
}
