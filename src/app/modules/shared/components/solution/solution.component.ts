import {ActivatedRoute, Router} from '@angular/router';
import {Component, OnInit} from '@angular/core';
import {Solution} from '../../../../models/solution.model';
import {SolutionService} from '../../../../services/solution.service';
import {SendMessageModalComponent} from '../send-message-modal/send-message-modal.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {User} from '../../../../models/user.model';
import {Module} from '../../../../models/module.model';
import {Problem} from '../../../../models/problem.model';
import {Course} from '../../../../models/course.model';

@Component({
  selector: 'app-solution',
  templateUrl: './solution.component.html',
  styleUrls: ['./solution.component.css']
})
export class SolutionComponent implements OnInit {

  public solution: Solution;
  public readonly isStudent = User.is(User.ROLE_STUDENT);

  constructor(protected route: ActivatedRoute,
              private router: Router,
              protected solutionService: SolutionService,
              private modal: NgbModal) {
  }

  ngOnInit() {
    Solution.tested$.subscribe((solutionId) => {
      if (this.solution.id === solutionId) {
        this.loadSolution(this.solution.id, this.solution.problem_id, this.solution.module_id, this.solution.course_id);
      }
    });
    this.route.params.subscribe(route => {
      this.loadSolution(route['solution_id'], route['problem_id'], route['module_id'], route['course_id']);
    })
  }

  discuss() {
    this.modal.open(SendMessageModalComponent).componentInstance.solutionId = this.solution.id;
  }

  storeSolution() {
      this.solutionService.store(this.solution)
        .subscribe((solution: Solution) => {
          const url_parts = this.router.url.split('/');
          const url = url_parts.slice(0, url_parts.length - 1).join('/') + '/' + solution.id;

          this.router.navigate([url]);
        });

  }

  protected loadSolution(solution_id, problem_id, module_id, course_id) {
    this.solutionService.show(solution_id, problem_id, module_id).subscribe(solution => {
      this.solution = solution;
      this.solution.module = new Module({'id': module_id});
      this.solution.problem = new Problem({'id': problem_id});
      this.solution.course = new Course({'id': course_id});
    });
  }
}
