import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Problem} from '../../../../models/problem.model';
import {Module} from '../../../../models/module.model';
import {Course} from '../../../../models/course.model';
import {CourseService} from '../../../../services/course.service';
import {ProblemService} from '../../../../services/problem.service';
import {ModuleService} from '../../../../services/module.service';
import {Solution} from '../../../../models/solution.model';
import {SolutionService} from '../../../../services/solution.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SendMessageModalComponent} from '../send-message-modal/send-message-modal.component';
import {User} from '../../../../models/user.model';

@Component({
  selector: 'app-problem',
  templateUrl: './problem.component.html',
  styleUrls: ['./problem.component.css']
})
export class ProblemComponent implements OnInit {

  public problem: Problem;

  public solutions: Solution[] = [];

  public readonly isStudent = User.is(User.ROLE_STUDENT);
  public readonly fileReader = new FileReader;
  public newSolution: Solution = new Solution;

  public codeFromFile: string;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private problemService: ProblemService,
              private solutionService: SolutionService,
              private modal: NgbModal) {

    this.fileReader.onload = (event) => {
      this.codeFromFile = this.fileReader.result;
    }
  }

  ngOnInit() {
    this.problemService.show(+this.route.snapshot.params['problem_id'], +this.route.snapshot.params['module_id'])
      .subscribe(problem => {
        this.problem = problem;
        this.newSolution = new Solution({
          problem: this.problem,
          module: new Module({id: +this.route.snapshot.params['module_id']}),
          course: new Course({id: +this.route.snapshot.params['course_id']}),
          programming_language: problem.programming_languages[0],
          code: ''
        });
      });
    this.getPreviousSolutions();
  }

  getPreviousSolutions(page = 1) {
    this.solutionService.index({
      page: page,
      problem_id: +this.route.snapshot.params['problem_id'],
      module_id: +this.route.snapshot.params['module_id'],
    })
      .subscribe(solutions => {
        this.solutions = solutions;
      });
  }

  onSolutionSubmit() {
    this.solutionService.store(this.newSolution)
      .subscribe((solution: Solution) => {
        this.router.navigate([
          'solutions', solution.id
        ], {relativeTo: this.route});
      });
  }

  onSolutionFileAdded(event) {
    this.fileReader.readAsText(event.srcElement.files[0])
  }

  discuss() {
    this.modal.open(SendMessageModalComponent).componentInstance.problemId = this.problem.id;
  }

  pageChange(page) {
    this.getPreviousSolutions(page);
  }
}
