import {AfterViewInit, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
declare const ace: any;
@Component({
  selector: 'app-ace-editor',
  templateUrl: './ace-editor.component.html',
  styleUrls: ['./ace-editor.component.css']
})
export class AceEditorComponent implements OnInit, AfterViewInit {

  protected aceEditor: any;
  protected _mode: string;
  @Input() set mode(value: string) {
    this._mode = value;
    this.updateEditor();
  }

  protected _theme = 'monokai';
  @Input() set theme(value: string) {
    this._theme = value;
    this.updateEditor();
  }

  @ViewChild('editor') protected editor: ElementRef;

  protected _readOnly = false;
  @Input() set readOnly(value: boolean) {
    this._readOnly = value;
    this.updateEditor();
  }

  protected _code = '';
  @Input() set code(val) {
    this._code = val;
    if (this.aceEditor) {
      this.aceEditor.setValue(this._code || '');
    }
  };

  @Output() readonly codeChange$ = new EventEmitter();

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.aceEditor = ace.edit(this.editor.nativeElement);
    this.aceEditor.getSession().on('change', e => this.codeChange$.emit(this.aceEditor.getValue()));
    this.aceEditor.setValue(this._code || '');
    this.updateEditor();
  }

  protected updateEditor() {
    if (this.aceEditor && this._mode) {
      this.aceEditor.getSession().setMode('ace/mode/' + this._mode);
      this.aceEditor.setTheme('ace/theme/' + this._theme);
      this.aceEditor.setReadOnly(this._readOnly);
    }
  }
}
