import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {User} from '../../../../models/user.model';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {

  public user = new User;
  public canEdit: boolean;
  public showNotes: boolean;

  constructor(private route: ActivatedRoute, private userService: UserService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(p => {
      this.userService.show(+p.get('user_id'))
        .subscribe(u => {
          this.user = u;
          this.showNotes = User.is(User.ROLE_CURATOR);
          this.canEdit = User.get().canEdit(u);
        });
    });
  }
}
