import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {SelectOption} from '../../../../models/select-option.interface';

@Component({
  selector: 'app-select-input-field',
  templateUrl: './select-input-field.component.html',
  styleUrls: ['./select-input-field.component.css']
})
export class SelectInputFieldComponent implements OnInit {
  @Input() selected?: string;
  @Output() selectedChange = new EventEmitter<string>();
  @Input() options: SelectOption[];

  @Input() errors: string[];
  @Input() label: string;

  constructor() {
  }

  ngOnInit() {
  }

  change($event) {
    this.selectedChange.emit($event.target.value);
  }
}
