import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {UserService} from '../../../../services/user.service';
import {UserProfile} from '../../../../models/user-profile.model';

@Component({
  selector: 'app-profile-edit',
  templateUrl: './profile-edit.component.html',
  styleUrls: ['./profile-edit.component.css']
})
export class ProfileEditComponent implements OnInit {
  public profile: UserProfile = new UserProfile;
  public success_save = false;
  public errors: any = {};
  public image: Blob;

  private userId: number;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private userService: UserService) {
  }

  ngOnInit() {
    this.route.paramMap.subscribe(p => {
      this.userId = +p.get('user_id');
      this.userService.show(this.userId)
        .subscribe(u => this.profile = <UserProfile>u);
    });
  }

  imageChange(image: Blob) {
    this.image = image;
  }

  onSubmit() {
    this.userService.update(this.userId, this.profile, this.image)
      .subscribe(
        response => {
          this.success_save = true;
          this.router.navigate(['/profile', this.userId]);
          this.errors = [];
        },
        errors => {
          this.errors = errors;
        }
      );
  }
}
