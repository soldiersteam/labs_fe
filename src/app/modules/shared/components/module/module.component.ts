import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Module} from '../../../../models/module.model';
import {ModuleService} from '../../../../services/module.service';
import {ProblemService} from '../../../../services/problem.service';
import {SolutionService} from '../../../../services/solution.service';
import {Problem} from '../../../../models/problem.model';
import {Solution} from '../../../../models/solution.model';

@Component({
  selector: 'app-module',
  templateUrl: './module.component.html',
  styleUrls: ['./module.component.css']
})
export class ModuleComponent implements OnInit {
  module: Module;

  constructor(private route: ActivatedRoute,
              private moduleService: ModuleService) {
  }

  ngOnInit() {
    this.moduleService.show(+this.route.snapshot.params['module_id'], true)
      .subscribe((module: Module) => {
        this.module = module;
      });
  }
}
