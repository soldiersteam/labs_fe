import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CourseService} from '../../../../services/course.service';
import {Course} from '../../../../models/course.model';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements OnInit {
  course: Course = new Course;

  constructor(private route: ActivatedRoute,
              private courseService: CourseService) {
  }

  ngOnInit() {
    this.courseService.show(+this.route.snapshot.params['course_id'])
      .subscribe((course: Course) => {
        this.course = course;
      });
  }
}
