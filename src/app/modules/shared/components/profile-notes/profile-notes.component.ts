import {Component, Input, OnInit} from '@angular/core';
import {ProfileNotesService} from '../../../../services/profile-notes.service';
import {UserNote} from '../../../../models/user-note.model';
import {User} from '../../../../models/user.model';

@Component({
  selector: 'app-profile-notes',
  templateUrl: './profile-notes.component.html',
  styleUrls: ['./profile-notes.component.css']
})
export class ProfileNotesComponent implements OnInit {

  @Input() userId: number;
  notes: UserNote[];
  visitorId = User.get().id;
  message: string;
  errors: any = {};

  constructor(private service: ProfileNotesService) {
  }

  ngOnInit() {
    this.service.index(this.userId)
      .subscribe(n => this.notes = n);
  }

  post() {
    this.service.post(this.message, this.userId)
      .subscribe(
        n => {
          this.notes.push(n);
          this.errors = {};
          this.message = '';
        },
        e => this.errors = e
      );
  }

  remove(id: number) {
    this.service.remove(id)
      .subscribe(r => this.notes = this.notes.filter(note => +note.id !== +id));
  }
}
