import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

@Component({
  selector: 'app-input-field',
  templateUrl: './input-field.component.html',
  styleUrls: ['./input-field.component.css']
})
export class InputFieldComponent implements OnInit {

  @Input() data: string;
  @Output() dataChange = new EventEmitter<string>();

  @Input() errors: string[];
  @Input() label: string;
  @Input() type = 'text';
  @Input() placeholder = '';

  constructor() {
  }

  ngOnInit() {
  }

  change($event) {
    if (this.type !== 'file') {
      this.data = $event.target.value;
    } else {
      this.dataChange.emit($event.srcElement.files);
    }
  }
}
