import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {AceEditorComponent} from './components/ace-editor/ace-editor.component';
import {FormsModule} from '@angular/forms';
import {CourseComponent} from './components/course/course.component';
import {ModuleComponent} from './components/module/module.component';
import {ProblemComponent} from './components/problem/problem.component';
import {SolutionComponent} from './components/solution/solution.component';
import {ModulePreviewComponent} from './components/module-preview/module-preview.component';
import {RouterModule} from '@angular/router';
import {ConversationsComponent} from './components/conversations/conversations.component';
import {ConversationComponent} from './components/conversation/conversation.component';
import {SendMessageModalComponent} from './components/send-message-modal/send-message-modal.component';
import {SendMessageFormComponent} from './components/send-message-form/send-message-form.component';
import {MultiSelectComponent} from './components/multi-select/multi-select.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SortHeadComponent} from './components/sort-head/sort-head.component';
import {TranslatePipe} from './pipes/translate.pipe';
import {LoaderComponent} from './components/loader/loader.component';
import {ProfileComponent} from './components/profile/profile.component';
import {ProfileEditComponent} from './components/profile-edit/profile-edit.component';
import {InputFieldComponent} from './components/input-field/input-field.component';
import {ImageUploadComponent} from './components/image-upload/image-upload.component';
import {ProfileNotesComponent} from './components/profile-notes/profile-notes.component';
import {ImageCropperModule} from 'ng2-img-cropper';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {ConversationPreviewComponent} from './components/conversation-preview/conversation-preview.component';
import {MessageDatePipe} from './pipes/messageDate.pipe';
import {SelectInputFieldComponent} from './components/select-input-field/select-input-field.component';

@NgModule({
  imports: [
    NgbModule,
    CommonModule,
    FormsModule,
    RouterModule,
    ImageCropperModule,
    NgxChartsModule,
  ],
  declarations: [
    AceEditorComponent,
    CourseComponent,
    ModuleComponent,
    ProblemComponent,
    SolutionComponent,
    ModulePreviewComponent,
    ConversationsComponent,
    ConversationComponent,
    SendMessageModalComponent,
    SendMessageFormComponent,
    MultiSelectComponent,
    SortHeadComponent,
    LoaderComponent,
    TranslatePipe,
    ProfileComponent,
    ProfileEditComponent,
    InputFieldComponent,
    ImageUploadComponent,
    ProfileNotesComponent,
    ConversationPreviewComponent,
    MessageDatePipe,
    SelectInputFieldComponent,
  ],
  exports: [
    CommonModule,
    NgbModule,
    FormsModule,
    AceEditorComponent,
    MultiSelectComponent,
    SortHeadComponent,
    LoaderComponent,
    TranslatePipe,
    ConversationsComponent,
    NgxChartsModule,
    MessageDatePipe,
    InputFieldComponent,
    SelectInputFieldComponent
  ],
  entryComponents: [
    SendMessageModalComponent
  ],
  providers: [
    TranslatePipe,
    MessageDatePipe
  ]
})
export class SharedModule {
}
