import {Pipe, PipeTransform} from "@angular/core";
import * as moment from 'moment';

@Pipe({
    name: 'messageDate',
    pure: false
})
export class MessageDatePipe implements PipeTransform {

    transform(date: string, ...args): any {

      const momentDate = moment(date);
      const startWeekDate = moment().startOf('isoWeek');
      const startDayDate = moment().startOf('day');

      if (momentDate.isAfter(startDayDate)) {
        return momentDate.format('LT');
      } else if (momentDate.isAfter(startWeekDate)) {
        return momentDate.format('dddd');
      }
      return momentDate.format('l');
    }
}
