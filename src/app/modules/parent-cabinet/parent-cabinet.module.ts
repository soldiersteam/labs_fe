import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MainComponent} from './components/main/main.component';
import {ChildrenComponent} from './components/children/children.component';
import {RouterModule} from '@angular/router';
import {parentRoutes} from './parent-cabinet.routes';
import {AddChildComponent} from './components/add-child/add-child.component';
import {FormsModule} from '@angular/forms';
import {AddChildService} from './services/add-child.service';
import {ChildService} from './services/child.service';
import {CourseComponent} from './components/courses/courses.component';
import {CourseBuyResultComponent} from './components/course-buy-result/course-buy-result.component';
import {CourseBuyComponent} from './components/course-buy/course-buy.component';
import {CourseService} from './services/course.service';
import {SharedModule} from '../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    SharedModule,
    RouterModule.forChild(parentRoutes)
  ],
  providers: [
    AddChildService,
    ChildService,
    CourseService
  ],
  declarations: [
    MainComponent,
    ChildrenComponent,
    AddChildComponent,
    CourseComponent,
    CourseBuyComponent,
    CourseBuyResultComponent,
  ]
})
export class ParentCabinetModule {
}
