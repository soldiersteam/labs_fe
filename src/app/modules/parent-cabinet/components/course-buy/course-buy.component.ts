import {Component, OnInit, ViewContainerRef, Renderer2, Input} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {CourseService} from '../../services/course.service';
import {Course} from '../../../../models/course.model';
import {CourseBuy} from '../../../../models/courseBuy.model';

declare const LiqPayCheckout: any;

@Component({
  selector: 'app-course-buy',
  templateUrl: 'course-buy.component.html',
  styleUrls: ['course-buy.component.css']
})
export class CourseBuyComponent implements OnInit {

  @Input() public course: Course;
  protected studentId: number;

  constructor(private container: ViewContainerRef,
              private renderer2: Renderer2,
              private courseService: CourseService,
              private route: ActivatedRoute,
              private router: Router) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(p => this.studentId = p.studentId);
    if (!this.course) {
      this.courseService.show(this.route.snapshot.params['course_id'])
        .subscribe((response: Course) => {
          this.course = response;
        });
    }
  }

  buy() {
    this.courseService.getBuyData(this.course.id, this.studentId)
      .subscribe((courseBuy: CourseBuy) => {
        (<any>window).LiqPayCheckoutCallback = () => {
          LiqPayCheckout.init({
            data: courseBuy.data,
            signature: courseBuy.signature,
            // embedTo: "#liqpay_checkout",
            mode: 'popup' // embed || popup
          }).on('liqpay.callback', data => {
            if (data.status === 'success' || data.status === 'sandbox') {
              this.router.navigate(['/parent', 'courses', this.course.id, 'buy-result']);
            }
          }).on('liqpay.ready', function (data) {
            // ready
          }).on('liqpay.close', function (data) {
            // close
          });
        };

        const s = this.renderer2.createElement('script');
        s.src = `//static.liqpay.com/libjs/checkout.js`;
        this.renderer2.appendChild(this.container.element.nativeElement, s);
      });
  }
}
