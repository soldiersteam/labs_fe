import {Component, NgZone, OnInit} from '@angular/core';
import {ChildService} from '../../services/child.service';
import {User} from '../../../../models/user.model';
import {TranslatePipe} from '../../../shared/pipes/translate.pipe';

@Component({
  selector: 'app-children',
  templateUrl: './children.component.html',
  styleUrls: ['./children.component.css']
})
export class ChildrenComponent implements OnInit {

  public children: User[] = [];

  chartData = {};

  view = undefined;
  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  showXAxisLabel = false;
  xAxisLabel = '';
  showYAxisLabel = false;
  yAxisLabel = '';
  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };
  // line, area
  autoScale = true;

  constructor(
    protected childService: ChildService,
    protected translatePipe: TranslatePipe
  ) {
    this.xAxisLabel = translatePipe.transform('Date');
    this.yAxisLabel = translatePipe.transform('Progress');
  }

  ngOnInit() {
    this.childService.index()
      .subscribe(children => {
        this.children = children;
        this.children.forEach(el => {
          this.chartData[el.id] = el.chartData;
        });
      });
  }

  schedule(child: User) {
    child.is_scheduled = true;
    this.childService.schedule(child.id)
      .subscribe();
  }

}
