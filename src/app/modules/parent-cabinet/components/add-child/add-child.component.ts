import {Component, OnInit} from '@angular/core';
import {environment} from '../../../../../environments/environment';
import {AddChildService} from '../../services/add-child.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-add-child',
  templateUrl: './add-child.component.html',
  styleUrls: ['./add-child.component.css']
})
export class AddChildComponent implements OnInit {

  public credentials = {
    name: '',
    email: '',
    password: '',
    password_confirmation: '',
    success_url: environment.site_url + '/email-validation'
  };
  public errors: any = {};

  constructor(protected addChild: AddChildService, protected router: Router) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.addChild.register(this.credentials)
      .subscribe(
        response => {
          this.router.navigate(['/parent']);
        },
        errors => {
          this.errors = errors;
        }
      );
  }

}
