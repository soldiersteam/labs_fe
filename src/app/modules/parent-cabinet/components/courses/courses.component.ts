import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {CourseService} from '../../services/course.service';
import {Course} from '../../../../models/course.model';

@Component({
  selector: 'app-course',
  templateUrl: './courses.component.html',
  styleUrls: ['./courses.component.css']
})
export class CourseComponent implements OnInit {

  public courses: Course[] = [];
  public studentId: number;

  constructor(protected route: ActivatedRoute, protected courseService: CourseService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(p => {
      if (p.studentId) {
        this.studentId = p.studentId;
        this.courseService.indexForStudent(p.studentId).subscribe(c => this.courses = c);
      }
    })
  }
}
