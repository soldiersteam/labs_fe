import {Routes} from '@angular/router';
import {ChildrenComponent} from './components/children/children.component';
import {AddChildComponent} from './components/add-child/add-child.component';
import {CourseComponent} from './components/courses/courses.component';
import {CourseBuyResultComponent} from './components/course-buy-result/course-buy-result.component';
import {CourseBuyComponent} from './components/course-buy/course-buy.component';

/**
 * Created by dima on 6/22/17.
 */
export const parentRoutes: Routes = [
  {
    path: '',
    children: [
      // {path: '', component: MainComponent},
      {path: '', redirectTo: 'children', pathMatch: 'full'},
      {path: 'children', component: ChildrenComponent},
      {path: 'add-child', component: AddChildComponent},
      {
        path: 'courses', children: [
        {
          path: '',
          component: CourseComponent,
        },
        {
          path: ':course_id', children: [
          {
            path: 'buy',
            component: CourseBuyComponent,
            data: {title: 'Course'},
          },
          {
            path: 'buy-result',
            component: CourseBuyResultComponent,
            data: {title: 'Course'}
          }]
        },
      ]
      },
    ]
  }
];
