import {BaseService} from '../../../services/base.service';
/**
 * Created by dima on 6/25/17.
 */
export abstract class BaseParentService extends BaseService {

  protected url(action: string, params: any = null): string {
    return super.url('parent/' + action, params);
  }
}
