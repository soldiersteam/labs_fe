import {Injectable} from '@angular/core';
import {BaseParentService} from './base-parent.service';

@Injectable()
export class AddChildService extends BaseParentService {

  public register(credentials: any) {
    return this.http.post(this.url('register'), credentials, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
