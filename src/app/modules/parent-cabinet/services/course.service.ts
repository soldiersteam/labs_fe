import {Injectable} from '@angular/core';
import {BaseParentService} from './base-parent.service';
import {Observable} from 'rxjs/Observable';
import {CourseBuy} from '../../../models/courseBuy.model';
import {Course} from '../../../models/course.model';

@Injectable()
export class CourseService extends BaseParentService {


  protected makeModel(data): Course {
    return new Course(data);
  }

  indexForStudent(studentId: number) {
    return this.http.get(this.url(`courses`), this.getRequestOptions({student_id: studentId}))
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  show(id: number): Observable<Course> {
    return this.http.get(this.url(`courses/${id}`), this.getRequestOptions())
      .map(this.extractData)
      .map(this.makeModel)
      .catch(this.handleError);
  }

  public getBuyData(id: number, studentId: number): Observable<CourseBuy> {
    return this.http.get(this.url(`courses/${id}/buy`), this.getRequestOptions({student_id: studentId}))
      .map(this.extractData)
      .catch(this.handleError);
  }
}
