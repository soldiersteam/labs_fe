import {Injectable} from '@angular/core';
import {BaseParentService} from './base-parent.service';
import {User} from '../../../models/user.model';

@Injectable()
export class ChildService extends BaseParentService {

  protected makeModel(data: any) {
    return new User(data);
  }

  index() {
    return this.http.get(this.url('children'), this.getRequestOptions())
      .map(this.extractData)
      .map(this.mapResourceData)
      .catch(this.handleError);
  }

  schedule(id: number) {
    return this.http.post(this.url(`children/${id}/schedule`), {}, this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }

  show(id: number) {
    return this.http.get(this.url(`children${id}`), this.getRequestOptions())
      .map(this.extractData)
      .catch(this.handleError);
  }
}
