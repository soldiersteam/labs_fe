import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {Http, HttpModule} from '@angular/http';
import {RouterModule} from '@angular/router';
import {appRoutes} from './app.routes';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {SimpleNotificationsModule} from 'angular2-notifications';

import {AppComponent} from './app.component';

import 'hammerjs';
import 'formdata-polyfill';
import {CoursePreviewComponent} from './components/course-preview/course-preview.component';
import {NavMenuComponent} from './components/nav-menu/nav-menu.component';
import {LanguagePickerComponent} from './components/language-picker/language-picker.component';
import {StandingsComponent} from './components/standings/standings.component';
import {LoginComponent} from './components/login/login.component';
import {RegisterComponent} from './components/register/register.component';
import {PassForgotComponent} from './components/pass-forgot/pass-forgot.component';
import {UserComponent} from './components/user/user.component';
import {UserPreviewComponent} from './components/user-preview/user-preview.component';
import {HomeComponent} from './components/home/home.component';
import {CourseAboutComponent} from './components/course-about/course-about.component';

import {EmailValidationComponent} from './components/email-validation/email-validation.component';
import {EmailValidationTokenComponent} from './components/email-validation-token/email-validation-token.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {ResetPasswordTokenComponent} from './components/reset-password-token/reset-password-token.component';


import {AuthService} from './services/auth.service';
import {CourseService} from './services/course.service';
import {ModuleService} from './services/module.service';
import {ProblemService} from './services/problem.service';
import {SolutionService} from './services/solution.service';
import {AuthGuard} from './guards/auth.guard';
import {NoAuthGuard} from './guards/no-auth.guard';
import {BreadcrumbsComponent} from './components/breadcrumbs/breadcrumbs.component';
import {BreadcrumbGuard} from './guards/breadcrumb.guard';
import {BreadcrumbService} from './services/breadcrumb.service';
import {ProgrammingLanguageService} from './services/programming-language.service';
import {UserService} from './services/user.service';
import {RegisterStudentComponent} from './components/register-student/register-student.component';
import {RegisterStudentService} from './services/register-student.service';
import {InviteSuccessComponent} from './components/invite-success/invite-success.component';
import {CuratorCabinetModule} from './modules/curator-cabinet/curator-cabinet.module';
import {ParentCabinetModule} from './modules/parent-cabinet/parent-cabinet.module';
import {StudentModule} from './modules/student/student.module';
import {SharedModule} from './modules/shared/shared.module';
import {ConversationService} from './services/conversation.service';
import {EchoService} from './services/echo.service';
import {MessageService} from './services/message.service';
import {TranslationService} from './services/translation.service';

import {InterceptedHttp} from './services/interceptedHttp';
import {LanguageService} from './services/language.service';
import {IsRoleGuard} from './guards/is-role.guard';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {NotificationsService} from 'angular2-notifications/dist';
import {CanEditProfileGuard} from './guards/can-edit-profile.guard';
import {HistoryComponent} from './components/history/history.component';
import {FaqComponent} from './components/faq/faq.component';
import {LangsPreviewComponent} from './components/langs-preview/langs-preview.component';
import {ProfileNotesService} from './services/profile-notes.service';


@NgModule({
  declarations: [
    AppComponent,
    CoursePreviewComponent,
    NavMenuComponent,
    LanguagePickerComponent,
    StandingsComponent,
    LoginComponent,
    RegisterComponent,
    PassForgotComponent,
    UserComponent,
    UserPreviewComponent,
    HomeComponent,
    CourseAboutComponent,
    EmailValidationComponent,
    EmailValidationTokenComponent,
    ResetPasswordComponent,
    ResetPasswordTokenComponent,
    BreadcrumbsComponent,
    RegisterStudentComponent,
    InviteSuccessComponent,
    HistoryComponent,
    FaqComponent,
    LangsPreviewComponent,
  ],
  imports: [
    NgbModule.forRoot(),
    BrowserAnimationsModule,
    SimpleNotificationsModule.forRoot(),
    BrowserModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    SharedModule
  ],
  providers: [
    AuthService,
    CourseService,
    ModuleService,
    ProblemService,
    SolutionService,
    BreadcrumbService,
    ProgrammingLanguageService,
    UserService,
    RegisterStudentService,
    ConversationService,
    EchoService,
    MessageService,
    LanguageService,
    TranslationService,
    NotificationsService,
    ProfileNotesService,

    AuthGuard,
    NoAuthGuard,
    BreadcrumbGuard,
    IsRoleGuard,
    CanEditProfileGuard,
    {
      provide: Http,
      useClass: InterceptedHttp
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
