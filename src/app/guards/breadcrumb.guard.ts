import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateChild} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {BreadcrumbService} from '../services/breadcrumb.service';
import {CourseService} from '../services/course.service';
import {ModuleService} from '../services/module.service';
import {ProblemService} from '../services/problem.service';
import {Breadcrumb} from '../models/breadcrumb.model';
import {SolutionService} from '../services/solution.service';
import {TranslatePipe} from '../modules/shared/pipes/translate.pipe';

@Injectable()
export class BreadcrumbGuard implements CanActivateChild {
  constructor(protected breadcrumb: BreadcrumbService,
              protected course: CourseService,
              protected module: ModuleService,
              protected problem: ProblemService,
              protected translatePipe: TranslatePipe,
              protected solution: SolutionService) {
  }

  canActivateChild(childRoute: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const crumbs = [];
    if (childRoute.params['course_id'] && childRoute.params['module_id']) {
      this.course.show(childRoute.params['course_id']).subscribe(c => {
        let urls = ['/student', 'courses', c.id];
        crumbs.push(new Breadcrumb(urls, c.name));
        this.module.show(childRoute.params['module_id'], false).subscribe(m => {
          urls = [...urls, 'modules', m.id];
          crumbs.push(new Breadcrumb(urls, m.name));
          if (childRoute.params['problem_id']) {
            this.problem.show(childRoute.params['problem_id'], childRoute.params['module_id']).subscribe(p => {
              urls = [...urls, 'problems', p.id];
              crumbs.push(new Breadcrumb(urls, p.name));
              if (childRoute.params['solution_id']) {
                this.solution.show(childRoute.params['solution_id'], childRoute.params['problem_id'], childRoute.params['module_id']).subscribe(s => {
                  urls = [...urls, 'solutions', s.id];
                  crumbs.push(new Breadcrumb(urls, this.translatePipe.transform('Solution') + ` #${s.id}`));
                });
              }
            });
          }
        });
      });
    }

    this.breadcrumb.update(crumbs);
    return true;
  }
}
