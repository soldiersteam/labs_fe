import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';
import {UserService} from '../services/user.service';
import {ReplaySubject} from 'rxjs/ReplaySubject';

@Injectable()
export class CanEditProfileGuard implements CanActivate {

  constructor(private router: Router, private userService: UserService) {

  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    const user = User.get();
    const source = new ReplaySubject<boolean>();
    this.userService.show(+next.paramMap.get('user_id')).subscribe(
      (u: User) => {
        const canEdit = user.canEdit(u);
        if (!canEdit) {
          this.router.navigate(User.defaultRedirectTo());
        }
        source.next(canEdit);
        source.complete();
      }
    );
    return source.asObservable();
  }
}
