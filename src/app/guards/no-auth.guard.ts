import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';

@Injectable()
export class NoAuthGuard implements CanActivate {
  constructor(protected router: Router) {
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (User.guest()) {
      return true;
    }
    this.router.navigate(User.defaultRedirectTo());
  }
}
