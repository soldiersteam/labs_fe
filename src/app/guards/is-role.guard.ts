import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs/Observable';
import {User} from '../models/user.model';

@Injectable()
export class IsRoleGuard implements CanActivate {

  constructor(protected router: Router) {
  }

  redirect() {
    this.router.navigate(User.defaultRedirectTo());
  }

  canActivate(next: ActivatedRouteSnapshot,
              state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    if (User.isAnyOf(next.data.roles)) {
      return true;
    }
    this.redirect();
    return false;
  }
}
