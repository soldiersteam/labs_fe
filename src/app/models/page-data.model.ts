/**
 * Created by dima on 6/14/17.
 */
export class PageData {
  constructor(public current_page?: number,
              public from?: number,
              public per_page?: number,
              public total?: number,
              public ended?: boolean) {
  }
}
