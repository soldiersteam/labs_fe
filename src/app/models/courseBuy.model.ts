/**
 * Created by dima on 5/31/17.
 */
import {Model} from './model';

export class CourseBuy extends Model {
  public data: string;
  public signature: string;

  get attributeMap() {
    return {
      data: String,
      signature: String
    };
  }
}
