import {Module} from './module.model';
import {TranslatableModel} from './translatableModel';

export class Course extends TranslatableModel {
  public id: number;
  public name: string;
  public description: string;
  public content: string;
  public price: number;
  public modules: Module[];

  get attributeMap() {
    return {
      id: Number,
      name: String,
      description: String,
      content: String,
      price: Number,
      modules: Module
    }
  }
}
