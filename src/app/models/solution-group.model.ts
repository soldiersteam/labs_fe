import {Model} from './model';
import {ProgrammingLanguage} from './programmingLanguage.model';
import {User} from './user.model';
import {Module} from './module.model';
import {Problem} from './problem.model';
import {Comment} from './comment.model'
import {Solution} from './solution.model';
import {Course} from './course.model';

export class SolutionGroup extends Model {

  static readonly FilterAttributes = ['course_id', 'module_id', 'programming_language_id', 'user_id', 'comment_id', 'problem_id'];

  get attributeMap(): { [p: string]: any } {
    return {
      courses_name: String,
      course_id: Number,

      users_name: String,
      user_id: Number,

      modules_name: String,
      module_id: Number,

      problems_name: String,
      problem_id: Number,

      programming_languages_title: String,
      programming_language_id: Number,

      comments_text: String,
      comment_id: Number,
      comment: Comment
    };
  }

  courses_name: string;
  course_id: number;

  users_name: string;
  user_id: number;

  modules_name: string;
  module_id: number;

  problems_name: string;
  problem_id: number;

  programming_languages_title: string;
  programming_language_id: number;

  comments_text: string;
  comment_id: number;

  solutions: Solution[];

  comment: Comment;

  open = false;

  public constructor(data: any = {}) {
    super(data);
    if (!this.solutions) {
      this.solutions = [];
    }
  }

  public get filterAttributes() {
    const obj = {};
    for (const attr of SolutionGroup.FilterAttributes) {
      obj[attr] = this[attr];
    }
    return obj;
  }

}
