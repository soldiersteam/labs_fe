import {Model} from './model';

export class Language extends Model {
  public id: number;
  public name: string;
  public code: string;

  get attributeMap() {
    return {
      id: Number,
      name: String,
      code: String
    }
  }

  public static getCurrentLanguageCode() {

    const code = localStorage.getItem('languageCode');
    if (code) {
      return code;
    }
    return navigator.language.slice(0, 2).toLowerCase();
  }

  public static setLanguage(language: Language) {
    localStorage.setItem('languageCode', language.code);
  }

}
