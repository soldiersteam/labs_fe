/**
 * Created by dima on 6/12/17.
 */

export abstract class Model {
  abstract get attributeMap(): { [s: string]: any };

  public constructor(data: any = {}) {
    const attrMap = this.attributeMap;
    for (const attribute in attrMap) {
      if (attrMap.hasOwnProperty(attribute) && data.hasOwnProperty(attribute)) {
        if (data[attribute] instanceof Array) {
          this[attribute] = data[attribute].map(el => this.fillAttribute(el, attrMap[attribute]));
        } else {
          this[attribute] = this.fillAttribute(data[attribute], attrMap[attribute]);
        }
      }
    }
  }

  private fillAttribute(attr, model) {
    if (attr instanceof Model) {
      return attr;
    }
    return new model(attr).valueOf();
  }
}
