import {Model} from './model';

export class ProgrammingLanguage extends Model {

  public id: number;
  public title: string;
  public ace_mode: string;

  get attributeMap(): { [p: string]: any } {
    return {
      id: Number,
      title: String,
      ace_mode: String
    };
  }
}
