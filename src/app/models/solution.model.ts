import {Problem} from './problem.model';
import {Module} from './module.model';
import {Course} from './course.model';
import {SolutionReport} from './solutionReport.model';
import {ProgrammingLanguage} from './programmingLanguage.model';
import {Model} from './model';
import {User} from './user.model';
import {Status} from './status.model';
import {EventEmitter} from '@angular/core';
import {isArray} from 'util';

export class Solution extends Model {

  public static readonly tested$ = new EventEmitter<number>();

  public problem_id: number;
  public module_id: number;
  public course_id: number;
  public programming_language_id: number;
  public id: number;
  public success_percentage: number | null;
  public statuses: Status[];
  public state: string;
  public code: string;
  public created_at: string;
  public max_memory: number;
  public max_time: number;
  public user_id: number;
  public successful_reports: number;
  public user: User;
  public problem: Problem;
  public module: Module;
  public course: Course;
  public programming_language: ProgrammingLanguage;
  public reports: SolutionReport[];


  get attributeMap(): { [p: string]: any } {
    return {
      problem_id: Number,
      module_id: Number,
      course_id: Number,
      programming_language_id: Number,
      id: Number,
      success_percentage: Number,
      statuses: Status,
      state: String,
      code: String,
      created_at: String,
      max_memory: Number,
      max_time: Number,
      user_id: Number,
      successful_reports: Number,
      user: User,
      problem: Problem,
      module: Module,
      course: Course,
      programming_language: ProgrammingLanguage,
      reports: SolutionReport
    };
  }

  public isTested() {
    return this.state === 'tested';
  }

  get status() {
    return this.statuses.length ? this.statuses[0] : new Status({code: '-'})
  }
}
