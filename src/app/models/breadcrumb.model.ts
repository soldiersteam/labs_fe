/**
 * Created by dima on 6/14/17.
 */
export class Breadcrumb {
  constructor(public url: (number|string)[], public title: string) {
  }
}
