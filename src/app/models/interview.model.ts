import {Model} from './model';
import {User} from './user.model';

/**
 * Created by dima on 7/1/17.
 */
export class Interview extends Model {
  public id: number;
  public completed: boolean;
  public curator_id: number;
  public student_id: number;
  public created_at: string;
  public updated_at: string;
  public student: User;

  get attributeMap() {
    return {
      id: Number,
      completed: Boolean,
      curator_id: Number,
      student_id: Number,
      created_at: String,
      updated_at: String,
      student: User,
    }
  }
}
