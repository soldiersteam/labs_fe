import {Model} from './model';
import {EventEmitter} from '@angular/core';
import {UserProgress} from './user-progress';
import {environment} from '../../environments/environment';

export class User extends Model {

  static readonly ROLE_STUDENT = 'user';
  static readonly ROLE_PARENT = 'parent';
  static readonly ROLE_CURATOR = 'curator';
  static readonly ROLE_ADMIN = 'admin';

  static readonly login$ = new EventEmitter();
  static readonly logout$ = new EventEmitter();
  static model: User;

  public id: number;
  public name: string;
  public email: string;
  public token: string;
  public role: string;
  public avatar: string;
  public parent_id: number;
  public created_at: string;

  public is_scheduled: boolean | null;
  public can_buy: boolean | null;
  public course_id;

  public progress: UserProgress;

  get attributeMap(): { [p: string]: any } {
    return {
      id: Number,
      name: String,
      email: String,
      token: String,
      role: String,
      avatar: String,
      parent_id: Number,
      created_at: String,
      is_scheduled: Boolean,
      can_buy: Boolean,
      course_id: Number,
      progress: UserProgress
    };
  }

  public static deleteFromLS(): void {
    return localStorage.removeItem('user');
  }

  public static logout(): void {
    User.model = null;
    this.deleteFromLS();
    User.logout$.emit();
  }

  public static get(): User {
    if (User.model) {
      return User.model;
    }
    const userJSON = localStorage.getItem('user');
    if (userJSON) {
      return new User(JSON.parse(userJSON));
    }
    return null;
  }

  public static getToken(): string {
    const user = User.get();
    return user ? user.token : null;
  }

  public static set(user: User, remember: boolean): void {
    User.model = user;
    if (remember) {
      localStorage.setItem('user', JSON.stringify(user));
    }
    this.login$.emit(user);
  }

  public static guest(): boolean {
    return User.get() === null;
  }

  public static is(role: string): boolean {
    return User.guest() ? false : User.get().is(role);
  }


  public static isAnyOf(roles: string[]) {
    return User.guest() ? false : roles.indexOf(User.get().role) !== -1;
  }

  public static defaultRedirectTo(): string[] {
    if (User.guest()) {
      return ['/'];
    }
    switch (User.get().role) {
      case User.ROLE_STUDENT:
        return ['/student'];
      case User.ROLE_CURATOR:
        return ['/curator'];
      case User.ROLE_PARENT:
        return ['/parent'];
      default:
        User.deleteFromLS();
        window.location.href = environment.admin_url;
    }
  }

  public static id() {
    const user = User.get();
    return user ? user.id : null;
  }

  public get chartData() {
    return [{
      'name': 'Solutions',
      'series': this.progress.solutionsActivity
    }];
  }

  public static get noAvatarPicture() {
    return environment.host_url + '/no-avatar.png';
  }

  public is(role: string): boolean {
    return this.role === role;
  }

  public canEdit(user: User) {
    const curator = this.is(User.ROLE_CURATOR) && +user.id === +this.id;
    const parentStudent = this.is(User.ROLE_PARENT) && +user.parent_id === +this.id;
    return curator || parentStudent;
  }
}
