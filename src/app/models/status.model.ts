import {Model} from './model';

export class Status extends Model {
  public code: string;

  get attributeMap(): { [p: string]: any } {
    return {
      code: String
    };
  }
}
