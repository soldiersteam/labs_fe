import {Model} from './model';
import {SolutionActivity} from './solution-activity.model';

export class UserProgress extends Model {
  public courseProgress: number;
  public solutionsActivity: Array<SolutionActivity>;
  public academicPerformance: number;

  get attributeMap(): { [p: string]: any } {
    return {
      courseProgress: Number,
      solutionsActivity: SolutionActivity,
      academicPerformance: Number,
    };
  }

  public get academicPerformanceName(): string {
    let result = 'Bad academic performance';

    if (this.academicPerformance > 95) {
      result = 'Excellent academic performance';
    } else if (this.academicPerformance > 80) {
      result = 'Good academic performance';
    } else if (this.academicPerformance > 60) {
      result = 'Normal academic performance';
    }

    return result;
  }

  public get academicPerformanceColor(): string {
    let result = 'red';

    if (this.academicPerformance > 95) {
      result = 'green';
    } else if (this.academicPerformance > 80) {
      result = 'yellow';
    } else if (this.academicPerformance > 60) {
      result = 'orange';
    }

    return result;
  }
}
