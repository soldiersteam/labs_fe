import {Course} from './course.model';
import {Problem} from './problem.model';
import {TranslatableModel} from './translatableModel';

export class Module extends TranslatableModel {

  public id: number;
  public name: string;
  public description: string;
  public content: string;
  public user_progress: number;
  public course_id: number;
  public solved_problems_count: number;
  public problems_count: number;
  public course: Course;
  public problems: Problem[];

  get attributeMap() {
    return {
      id: Number,
      name: String,
      description: String,
      content: String,
      user_progress: Number,
      course_id: Number,
      solved_problems_count: Number,
      problems_count: Number,
      course: Course,
      problems: Problem
    }
  }

  get problemsCountArray(): any {
    return Array.from(Array(this.problems_count).keys());
  }
}
