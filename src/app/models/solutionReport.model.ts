import {Solution} from './solution.model';
import {Model} from './model';

export class SolutionReport extends Model {

  public id: number;
  public execution_time: number;
  public memory_peak: number;
  public status: string;
  public solution: Solution;


  get attributeMap(): { [p: string]: any } {
    return {
      id: Number,
      execution_time: Number,
      memory_peak: Number,
      status: String,
      solution: Solution
    };
  }
}
