import {User} from './user.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {SendMessageModalComponent} from '../modules/shared/components/send-message-modal/send-message-modal.component';
import {TranslatePipe} from '../modules/shared/pipes/translate.pipe';
import {environment} from '../../environments/environment';

/**
 * Created by dima on 6/22/17.
 */
export class NavMenuItem {
  title: string;
  link?: string;
  absolute?: boolean;
  handler?: () => void;

  public static getForUser(messageCount: number = 0, modal: NgbModal, translatePipe: TranslatePipe): any {
    if (User.guest()) {
      return [
        // {title: translatePipe.transform('Registration'), link: '/register'}
        {title: translatePipe.transform('Sign in'), link: '/login'},
        {title: translatePipe.transform('About project'), link: '/home'},
        {title: translatePipe.transform('Registration'), link: '/register'},
      ]
    }
    const shared = [
      {
        title: translatePipe.transform('Logout'),
        handler: () => {
          User.logout()
        }
      }
    ];
    switch (User.get().role) {
      case User.ROLE_ADMIN:
        return [...shared];
      case User.ROLE_CURATOR:
        return [
          {title: translatePipe.transform('Students'), link: '/curator/users'},
          {title: translatePipe.transform('Courses'), link: '/curator/courses'},
          {title: translatePipe.transform('Solutions'), link: '/curator/solutions'},
          {title: translatePipe.transform('Calendar'), link: '/curator/calendar'},
          {title: translatePipe.transform('Problems'), link: '/curator/problems'},
          {
            title: translatePipe.transform('Messages') + `(${messageCount})`,
            link: '/curator/messages'
          },
          {title: translatePipe.transform('Profile'), link: '/profile/' + User.get().id},
          ...shared,
        ];
      case User.ROLE_PARENT:
        return [
          {title: translatePipe.transform('My children'), link: '/parent/children'},
          ...shared
        ];
      case User.ROLE_STUDENT:
        return [
          {title: translatePipe.transform('Ask a question'), handler: () => modal.open(SendMessageModalComponent)},
          {title: translatePipe.transform('Previous courses'), link: '/student/owned-courses'},
          {title: translatePipe.transform('All solutions'), link: '/student/solutions'},
          {
            title: translatePipe.transform('Messages') + `(${messageCount})`,
            link: '/student/messages'
          },
          ...shared
        ];
      default:
        return [];
    }
  }
}
