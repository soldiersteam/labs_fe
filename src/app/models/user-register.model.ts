import {Model} from './model';

export class UserRegister extends Model {

  name: string;
  email: string;
  phone_number: string;
  role: string;
  password: string;
  password_confirmation: string;

  get attributeMap(): { [p: string]: any } {
    return {
      name: String,
      email: String,
      phone_number: String,
      role: String,
      password: String,
      password_confirmation: String,
    };
  }
}
