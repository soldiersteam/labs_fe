import {ProgrammingLanguage} from './programmingLanguage.model';
import {Solution} from './solution.model';
import {TranslatableModel} from './translatableModel';

export class Problem extends TranslatableModel {

  public id: number;
  public name: string;
  public difficulty: number;
  public image: string;
  public programming_languages: ProgrammingLanguage[];
  public solutions: Solution[];
  public solved: boolean;
  public display_id: string;


  get attributeMap(): { [p: string]: any } {
    return {
      id: Number,
      name: String,
      difficulty: Number,
      image: String,
      programming_languages: ProgrammingLanguage,
      solutions: Solution,
      solved: Boolean,
      display_id: String
    };
  }

  get localName(): string {
    return this.display_id || `${this.id}`;
  }

  get difficultyArray(): any {
    return Array(this.difficulty);
  }
}
