/**
 * Created by dima on 7/12/17.
 */

import {Model} from './model';
import {Conversation} from './conversation.model';
import {EventEmitter} from '@angular/core';
import {User} from './user.model';

export class Message extends Model {
  public static readonly received$ = new EventEmitter<Message>();
  public static readonly readCountUpdate$ = new EventEmitter<void>();
  public static receivedMessages: Message[] = [];

  public id: number;
  public message: string;
  public created_at: string;
  public user_id: number;
  public conversation_id: number;
  public conversation: Conversation;
  public user: User;
  public is_read: boolean;
  public show_user = true;

  public static readConversation(conversationId: number) {
    Message.receivedMessages = Message.receivedMessages.filter(m => +m.conversation_id !== +conversationId);
    this.readCountUpdate$.emit();
  }

  get attributeMap() {
    return {
      id: Number,
      message: String,
      created_at: String,
      user_id: Number,
      conversation_id: Number,
      conversation: Conversation,
      user: User,
      is_read: Boolean,
      show_user: Boolean
    }
  }
}

