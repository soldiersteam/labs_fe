import {Model} from './model';
import {EventEmitter} from '@angular/core';
import {User} from './user.model';
import {Message} from './message.model';

/**
 * Created by dima on 7/12/17.
 */

export class Conversation extends Model {

  public static readonly read$ = new EventEmitter<{ id: number, userId: number }>();
  public id: number;
  public name: string;
  public topic_type: string;
  public topic_id: number;
  public unread_count: number;
  public users: User[];
  public messages: Message[];

  get attributeMap() {
    return {
      id: Number,
      name: String,
      unread_count: Number,
      topic_type: Number,
      topic_id: Number,
      users: User,
      messages: Message
    };
  }
}
