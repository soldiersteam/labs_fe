import {Model} from './model';

export class SolutionActivity extends Model {
  name: string;
  value: number;

  get attributeMap(): { [p: string]: any } {
    return {
      name: String,
      value: Number
    };
  }
}
