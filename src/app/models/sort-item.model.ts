export class SortItem {

  public static readonly DIR_NONE = '';
  public static readonly DIR_DESC = 'desc';
  public static readonly DIR_ASC = 'asc';

  public static readonly DIR_ORDER = [
    SortItem.DIR_NONE,
    SortItem.DIR_DESC,
    SortItem.DIR_ASC
  ];

  public dir: string;

  public constructor(public title: string, protected prop: string) {
    this.reset();
  }

  public next() {
    const i = SortItem.DIR_ORDER.indexOf(this.dir);
    this.dir = i < SortItem.DIR_ORDER.length - 1 ? SortItem.DIR_ORDER[i + 1] : SortItem.DIR_ORDER[0];
  }

  public reset() {
    this.dir = SortItem.DIR_ORDER[0];
  }

  public getRequestData() {
    return this.dir !== SortItem.DIR_NONE ? {
      prop: this.prop,
      dir: this.dir
    } : {};
  }
}
