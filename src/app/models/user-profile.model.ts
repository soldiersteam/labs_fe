import {Model} from './model';

export class UserProfile extends Model {
  name: string;
  email: string;
  avatar: string;
  old_password?: string;
  password?: string;
  password_confirmation?: string;


  get attributeMap(): { [p: string]: any } {
    return {
      name: String,
      email: String,
      avatar: String,
      old_password: String,
      password: String,
      password_confirmation: String,
    };
  }
}
