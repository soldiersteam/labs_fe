import {Model} from './model';
import {User} from './user.model';

export class UserNote extends Model {
  id: number;
  message: string;
  created_at: string;
  author: User;


  get attributeMap(): { [p: string]: any } {
    return {
      id: Number,
      message: String,
      created_at: String,
      author: User
    };
  }
}
