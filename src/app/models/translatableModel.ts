import {Model} from "./model";
/**
 * Created by dima on 6/12/17.
 */
export abstract class TranslatableModel extends Model {

    public constructor(data: any = {}) {

        if (Array.isArray(data.translations) && data.translations.length) {
            data.translations.forEach((trans) => {
                data[trans.key] = trans.value;
            });
        }

        super(data);
    }
}
