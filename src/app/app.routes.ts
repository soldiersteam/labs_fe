import {Routes} from '@angular/router';
import {HomeComponent} from './components/home/home.component';
import {PassForgotComponent} from './components/pass-forgot/pass-forgot.component';
import {RegisterComponent} from './components/register/register.component';
import {LoginComponent} from './components/login/login.component';
import {CourseAboutComponent} from './components/course-about/course-about.component';
import {EmailValidationComponent} from './components/email-validation/email-validation.component';
import {EmailValidationTokenComponent} from './components/email-validation-token/email-validation-token.component';
import {ResetPasswordComponent} from './components/reset-password/reset-password.component';
import {ResetPasswordTokenComponent} from './components/reset-password-token/reset-password-token.component';
import {AuthGuard} from './guards/auth.guard';
import {NoAuthGuard} from './guards/no-auth.guard';
import {BreadcrumbGuard} from './guards/breadcrumb.guard';
import {RegisterStudentComponent} from './components/register-student/register-student.component';
import {InviteSuccessComponent} from './components/invite-success/invite-success.component';
import {ProfileComponent} from './modules/shared/components/profile/profile.component';
import {ProfileEditComponent} from './modules/shared/components/profile-edit/profile-edit.component';
import {IsRoleGuard} from './guards/is-role.guard';
import {User} from './models/user.model';
import {CanEditProfileGuard} from './guards/can-edit-profile.guard';

const appRoutes: Routes = [
  {
    path: '',
    canActivateChild: [BreadcrumbGuard],
    children: [
      {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
      },
      {
        path: 'course/:course_id/about',
        component: CourseAboutComponent,
        data: {title: 'About Course'}
      },
      {
        path: '',
        canActivate: [NoAuthGuard],
        children: [
          {
            path: 'home',
            component: HomeComponent,
            data: {title: 'Home'}
          },
          {
            path: 'invite-success',
            component: InviteSuccessComponent,
            data: {title: 'Приглашение отправлено'}
          },
          {
            path: 'register',
            component: RegisterComponent,
            data: {title: 'Registration'}
          },
          {
            path: 'login',
            component: LoginComponent,
            data: {title: 'Login'}
          },
          {
            path: 'email-validation',
            component: EmailValidationComponent,
            data: {title: 'Register next step'}
          },
          {
            path: 'email-validation/:token',
            component: EmailValidationTokenComponent,
            data: {title: 'Validate email'}
          },
          {
            path: 'pass-forgot',
            component: PassForgotComponent,
            data: {title: 'Forgot password'}
          },
          {
            path: 'reset-password',
            component: ResetPasswordComponent,
            data: {title: 'Reset password'}
          },
          {
            path: 'reset-password/:token',
            component: ResetPasswordTokenComponent,
            data: {title: 'Set new password'}
          },
        ]
      },
      {
        path: '',
        canActivate: [AuthGuard],
        children: [
          {
            path: 'profile/:user_id',
            children: [
              {
                path: '',
                component: ProfileComponent,
                data: {title: 'Profile'},
              },
              {
                path: 'edit',
                component: ProfileEditComponent,
                canActivate: [CanEditProfileGuard, IsRoleGuard],
                data: {roles: [User.ROLE_CURATOR, User.ROLE_PARENT], title: 'Edit profile'},
              }
            ]
          }
        ]
      }
    ]
  },
  {
    path: 'student',
    canActivate: [IsRoleGuard],
    data: {roles: [User.ROLE_STUDENT]},
    loadChildren: 'app/modules/student/student.module#StudentModule'
  },
  {
    path: 'parent',
    canActivate: [IsRoleGuard],
    data: {roles: [User.ROLE_PARENT]},
    loadChildren: 'app/modules/parent-cabinet/parent-cabinet.module#ParentCabinetModule'
  },
  {
    path: 'curator',
    canActivate: [IsRoleGuard],
    data: {roles: [User.ROLE_CURATOR]},
    loadChildren: 'app/modules/curator-cabinet/curator-cabinet.module#CuratorCabinetModule'
  }
  // { path: '**', component: PageNotFoundComponent }
];

export {appRoutes};
