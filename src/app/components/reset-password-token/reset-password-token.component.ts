import {Component, OnInit} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-reset-password-token',
  templateUrl: 'reset-password-token.component.html',
  styleUrls: ['reset-password-token.component.css']
})
export class ResetPasswordTokenComponent implements OnInit {


  public success: boolean;
  public token_exist: boolean;
  public credentials: any = {
    email: '',
    token: '',
    password: '',
    password_confirmation: ''
  };
  public errors: any = {};


  constructor(private route: ActivatedRoute,
              private authService: AuthService) {
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.credentials.token = params['token'];
      this.token_exist = true;
    });
  }

  onSubmit() {
    this.authService.resetPassword(this.credentials)
      .subscribe(
        response => {
          this.success = true;
        },
        errors => {
          this.errors = errors;
        }
      );
  }


}
