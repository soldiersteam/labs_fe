import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-pass-forgot',
  templateUrl: 'pass-forgot.component.html',
  styleUrls: ['pass-forgot.component.css']
})
export class PassForgotComponent implements OnInit {

  public credentials: any = {
    email: '',
    success_url: `${environment.site_url}/reset-password`
  };

  public errors: any = {};

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.sendResetToken(this.credentials)
      .subscribe(
        response => {
          this.router.navigate(['/reset-password']);
        },
        errors => {
          this.errors = errors;
        }
      );
  }
}
