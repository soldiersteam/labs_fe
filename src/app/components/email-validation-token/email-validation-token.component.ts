import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {ActivatedRoute} from '@angular/router';


@Component({
  selector: 'email-validation-token',
  templateUrl: 'email-validation-token.component.html',
  styleUrls: ['email-validation-token.component.css']
})

export class EmailValidationTokenComponent implements OnInit {

  public token: string;
  public success: boolean;

  public errors: any = {};


  constructor(private route: ActivatedRoute,
              private authService: AuthService,) {
  }


  ngOnInit() {
    this.route.params.subscribe(params => {
      this.token = params['token'];
    }); // (+) converts string 'id' to a number
    this.authService.emailValidation(this.token)
      .subscribe(
        response => this.success = true,
        errors => this.errors = errors
      );
  }
}
