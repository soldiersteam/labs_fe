import {Component, OnInit} from '@angular/core';
import {environment} from '../../../environments/environment';
import {RegisterStudentService} from '../../services/register-student.service';
import {Router} from '@angular/router';

@Component({
  selector: 'app-register-student',
  templateUrl: './register-student.component.html',
  styleUrls: ['./register-student.component.css']
})
export class RegisterStudentComponent implements OnInit {


  public readonly credentials = {
    name: '',
    parent_name: '',
    email: '',
    success_url: ''
  };

  public errors: any = {};

  constructor(protected registerStudent: RegisterStudentService, protected router: Router) {
  }

  onSubmit() {
    this.registerStudent.register({
      ...this.credentials,
      success_url: `${environment.site_url}/register`
    }).subscribe(r => this.router.navigate(['invite-success']), errors => this.errors = errors);
  }

  ngOnInit() {
  }
}
