import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Course} from "../../models/course.model";
import {CourseService} from "../../services/course.service";

@Component({
  selector: 'app-course-about',
  templateUrl: './course-about.component.html',
  styleUrls: ['./course-about.component.css']
})
export class CourseAboutComponent implements OnInit {
  course: Course;

  constructor(
    private route: ActivatedRoute,
    private courseService: CourseService
  ) {
  }

  ngOnInit() {
    this.courseService.show(+this.route.snapshot.params['course_id'])
      .subscribe((response: Course) => {
        this.course = response;
      });
  }
}
