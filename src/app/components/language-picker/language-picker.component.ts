import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {LanguageService} from '../../services/language.service';
import {Language} from '../../models/language.model';

@Component({
  selector: 'app-language-picker',
  templateUrl: './language-picker.component.html',
  styleUrls: ['./language-picker.component.css']
})
export class LanguagePickerComponent implements OnInit {

  public languages = [];
  public languageCode = null;

  constructor(private languageService: LanguageService) {
  }

  ngOnInit() {
    this.languageCode = Language.getCurrentLanguageCode();
    this.languageService.index().subscribe((languages: Language[]) => {
      this.languages = languages;
    });
  }

  public onChange(val) {
    Language.setLanguage(this.languages.find(lang => lang.code === this.languageCode));
    location.reload();
  }
}
