import {Component, OnInit} from '@angular/core';
import {BreadcrumbService} from '../../services/breadcrumb.service';
import {Breadcrumb} from '../../models/breadcrumb.model';

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html',
  styleUrls: ['./breadcrumbs.component.css']
})
export class BreadcrumbsComponent implements OnInit {

  public crumbs: Breadcrumb[] = [];

  constructor(protected breadcrumb: BreadcrumbService) {
  }

  ngOnInit() {
    this.breadcrumb.update$.subscribe(c => this.crumbs = c);
  }
}
