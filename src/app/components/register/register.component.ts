import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';
import {UserRegister} from '../../models/user-register.model';
import {SelectOption} from '../../models/select-option.interface';
import {User} from '../../models/user.model';
import {TranslatePipe} from '../../modules/shared/pipes/translate.pipe';

@Component({
  selector: 'app-register',
  templateUrl: 'register.component.html',
  styleUrls: ['register.component.css']
})
export class RegisterComponent implements OnInit {

  public credentials = new UserRegister({role: User.ROLE_STUDENT});
  public roles: SelectOption[];

  public errors: any = {};

  constructor(private router: Router,
              private authService: AuthService,
              private activatedRoute: ActivatedRoute,
              private translate: TranslatePipe) {
  }

  ngOnInit() {
    this.activatedRoute.queryParams.subscribe((p: any) => {
      this.credentials.name = p.name ? decodeURIComponent(p.name) : '';
      this.credentials.email = p.email ? decodeURIComponent(p.email) : '';
    });
    this.roles = [
      {key: User.ROLE_STUDENT, value: this.translate.transform('Student')},
      {key: User.ROLE_PARENT, value: this.translate.transform('Parent')},
    ]
  }

  onSubmit() {
    console.log(this.credentials);
    this.authService.register(this.credentials)
      .subscribe(
        response => {
          // this.router.navigate(['/login']);
        },
        errors => {
          this.errors = errors.errors || {};
        }
      );
  }
}
