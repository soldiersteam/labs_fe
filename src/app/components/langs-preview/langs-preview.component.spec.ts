import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LangsPreviewComponent } from './langs-preview.component';

describe('LangsPreviewComponent', () => {
  let component: LangsPreviewComponent;
  let fixture: ComponentFixture<LangsPreviewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LangsPreviewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LangsPreviewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
