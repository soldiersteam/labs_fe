import { Component, OnInit } from '@angular/core';
import {ProgrammingLanguageService} from '../../services/programming-language.service';
import {Observable} from 'rxjs';

@Component({
  selector: 'app-langs-preview',
  templateUrl: './langs-preview.component.html',
  styleUrls: ['./langs-preview.component.css']
})
export class LangsPreviewComponent implements OnInit {

  public currentLangLabel = '';
  protected languages = [];
  protected timer;

  constructor(protected programmingLanguageService: ProgrammingLanguageService) { }

  ngOnInit() {
    this.programmingLanguageService.index().subscribe((langs) => {
      this.languages = langs;
      this.timer = Observable.timer(0, 1000);
      this.timer.subscribe(t => this.currentLangLabel = langs[(Math.random() * (this.languages.length - 1)).toFixed(0)].title);
    });

  }

}
