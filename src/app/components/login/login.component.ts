import {Component, OnInit} from '@angular/core';
import {User} from '../../models/user.model';
import {Router} from '@angular/router';
import {AuthService} from '../../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css']
})
export class LoginComponent implements OnInit {

  public credentials = {
    login: '',
    password: '',
  };

  public remember = false;

  public errors: any = {};

  constructor(private router: Router,
              private authService: AuthService) {
  }

  ngOnInit() {
  }

  onSubmit() {
    this.authService.login(this.credentials.login, this.credentials.password)
      .subscribe(
        response => {
          User.set(new User({...response.user, token: response.api_token}), this.remember);
          const redirect = this.authService.redirectOnLogin === null ? User.defaultRedirectTo() : this.authService.redirectOnLogin;
          this.router.navigate(redirect);
        },
        errors => {
          this.errors = errors;
        }
      );
  }
}
