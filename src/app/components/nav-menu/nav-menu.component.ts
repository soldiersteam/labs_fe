import {Component, OnInit} from '@angular/core';
import {NavMenuItem} from '../../models/nav-menu-item.model';
import {User} from '../../models/user.model';
import {Message} from '../../models/message.model';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {TranslatePipe} from '../../modules/shared/pipes/translate.pipe';

@Component({
  selector: 'app-nav-menu',
  templateUrl: 'nav-menu.component.html',
  styleUrls: ['nav-menu.component.css']
})
export class NavMenuComponent implements OnInit {

  public isNavbarCollapsed: boolean;
  public items: NavMenuItem[] = [];


  constructor(private modal: NgbModal, private translatePipe: TranslatePipe) {
    this.isNavbarCollapsed = true;
    this.items = NavMenuItem.getForUser(Message.receivedMessages.length, modal, translatePipe);
    User.logout$
      .merge(User.login$)
      .merge(Message.received$)
      .merge(Message.readCountUpdate$)
      .subscribe(() => this.items = NavMenuItem.getForUser(Message.receivedMessages.length, modal, translatePipe));
  }

  ngOnInit() { }
}
