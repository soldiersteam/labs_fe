import {enableProdMode} from '@angular/core';
import {platformBrowserDynamic} from '@angular/platform-browser-dynamic';

import {AppModule} from './app/app.module';
import {environment} from './environments/environment';

if (environment.production) {
  enableProdMode();
}

declare const ace: any;
ace.config.set('basePath', '/ace');

platformBrowserDynamic().bootstrapModule(AppModule);
