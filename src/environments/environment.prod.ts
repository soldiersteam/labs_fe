export const environment = {
  production: true,
  api_url: 'https://api-qbit-online.dots.org.ua/api',
  host_url: 'https://api-qbit-online.dots.org.ua',
  site_url: 'https://qbit-online.dots.org.ua',
  socket_url: 'https://api-qbit-online.dots.org.ua',
  googleFormLink: 'https://docs.google.comK',
  admin_url: 'https://api-qbit-online.dots.org.ua',
};
