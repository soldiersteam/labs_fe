export const environment = {
  production: false,
  api_url: 'http://labs_be.dev:8080/api',
  host_url: 'http://labs_be.dev:8080',
  site_url: 'http://localhost:4200',
  socket_url: 'localhost:6001',
  googleFormLink: 'https://docs.google.com',
  admin_url: 'http://labs_be.dev:8080',
};
